#######################################
#									  #
# Main game script, written by X.Hunt #
#									  #
#######################################

"""
This is the bulk of the game.
The game loop is ran here, as well as all game logic.

Helper functions are called from other script, 
such as drawing to screen and playing sounds
"""

# Initialise the curses module
# Curses is used to draw to the screen

import locale
locale.setlocale(locale.LC_ALL, "")						# Sets the terminal up so we can use non-ASCII characters

import curses
mainScreen = curses.initscr()							# Create a screen object for the entire terminal screen
curses.start_color()									# Enable color
curses.noecho()											# We DO NOT want our keypresses to be shown in the screen
curses.curs_set(0)										# Hide the cursor

##########################

# Sets the terminal size to 25*80
import sys
sys.stdout.write("\x1b[8;25;80t")
curses.resizeterm(25, 80)

############################################################################################################################################################

# Other imports
import time												# Inbuilt time functions
import random											# Inbuilt random functions
import math												# Inbuilt maths functions
from pprint import pprint as pp 						# Inbuilt pretty print module, used for printing 2D arrays and dictionaries in an easy to read way
import sys, os, shutil									# System, OS and file manager modules

from Classes import Screen 								# Screen functions, written by X.Hunt
from Classes.DungeonGen import Dungeon					# Handles generation of dungeons, written by X.Hunt
from Classes.WorldGen import World						# Handles generation of the world, written by X.Hunt
from Classes.Log import log as Log 						# Log functions, written by X.Hunt
from Classes.Log import save2DArray as Save				# Save a 2D array to a file (used for debugging), written by X.Hunt
from Classes.Vector import Vector2 						# Vector2 class, written by X.Hunt, used for maths with Vectors made of 2 values
from Classes.SaveLoad import *							# Saving and loading class, written by X.Hunt, used to save and load data to and from file

from Entities.Monster import *							# Classes for Monsters, written by X.Hunt
from Entities.Mob import *								# Classes for Mobs, written by X.Hunt 			NOTE: Mobs are not implemented
from Entities.Player import Player as gamePlayer		# Player class, written by X.Hunt

from Items.Weapons import *								# Weapons class, written by X.Hunt
from Items.Armors import *								# Armors class, written by X.Hunt
from Items.Consumables import *							# Consumable items class, written by X.Hunt

from Classes.GetItem import getItem as GetItem			# Handles getting items from items defined in file
from Classes.GetItem import getAnyItem as GetAnyItem

############################################################################################################################################################

## Game class
class Game(object):
	"""
	Game object
	"""
	##########################

	# Static game metadata

	## Game title
	gameTitle = "Legend of Tenebrae"
	## Game version
	gameVersion = "v1"
	## How long should a game tick be?
	GAME_TICK = 0.1

	##########################

	def __init__(self):
		"""
		Class constructor
		"""

		# Game metatdata

		## Are we playing a game?
		self.isPlaying = False
		## What state are we in?						# -1: Not started, 0: In dungeon/overworld, 1: In battle 2: In inventory, 3: In gear
		self.gameState = -1
		## Are we in a dungeon?
		self.isInDungeon = False
		## Do we need to recalculate the game screen?
		self.recalcScreen = False

		##########################

		# Arrays for each screen

		## Array holding data for the game screen. This is passed to the function used for drawing the screen
		self.gameScreenArr = []
		## Array holding data for the side screen
		self.sideScreenArr = []
		## Array holding data for the lower screen
		self.lowerScreenArr = []

		## The total game array. This is cut to fit into the game screen array
		self.totalGameArr = []

		## A blank version of the side screen
		self.blankSideScreenArr = []

		##########################

		# Battle related data

		## Screen array used to draw the battle screen
		self.battleScreenArr = []
		## Current battle choice
		self.currentBattleChoice = 0

		##########################

		## Iventory screen
		self.inventScreenArr = []
		## Gear screen
		self.gearScreenArr = []
		## Player info screen
		self.playerInfoScreenArr = []
		## Current inventory choice
		self.currentInventoryChoice = 0
		## Current gear choice
		self.currentGearChoice = 2

		##########################

		# Other data

		## The player
		self.Player = None

		## List of all dungeons by name
		self.dungeonList = []
		## The current dungeon
		self.currentDun = None

		## The world
		self.world = None

		## List of all monsters
		self.allMonsters = []
		## List  of all mobs
		#self.allMobs = []			# NOTE: mobs are not implemented

		## The current tick
		self.CURRENT_GAME_TICK = 0

		## If there is a saved game
		self.isSavedGame = False

	############################################################################################################################################################

	## Update the screen
	def updateScreen(self):
		"""
		Updates all screens by passing their data array 
		to the Screen function responsable
		drawing that screen
		"""

		if(self.gameState == -1):
			return

		if(self.recalcScreen):
			self.calculateGameScreen()

		if(self.gameState == 1):
			self.updateBattleScreen()

		elif(self.gameState == 2):
			self.updateInventoryScreen()

		elif(self.gameState == 3):
			self.updateGearScreen()

		else:
			if(self.isPlaying):
				self.updatePlayerInfo()
				
		Screen.drawGame(self.gameScreenArr)
		Screen.drawSide(self.sideScreenArr)

	############################################################################################################################################################

	## Draws visible entities to the screen
	def drawEntites(self, startY, startX, endY, endX):
		"""
		Draws visible entities to the screen.

		NOTE: NOT USED IN FINAL CODE
		"""

		for mon in self.allMonsters:
			# Check the monster should be drawn

			monX = int(mon.position.x)
			monY = int(mon.position.y)

			if((monX < endX -1 and monX > startX +1) and (monY < endY -1 and monY > startY +1)):
				self.gameScreenArr[int(mon.position.y)][int(mon.position.x)] = [mon.char, "Red"]

	## Updates the side screen to show the inventory
	def updateInventoryScreen(self):
		self.inventScreenArr = [["INVENTORY", "White"],
								["", "Black"]
							   ]

		# If the current choice is greater than the size of the inventory, set it to the maximum size.
		# This occurs when the last item is used
		if(self.currentInventoryChoice > len(self.Player.inventory) -1):
			self.currentInventoryChoice = len(self.Player.inventory) -1

		# Iterate through to populate the screen array
		for i in xrange(len(self.Player.inventory)):
			text = self.Player.inventory[i].name

			if(i == self.currentInventoryChoice):
				text = "> " + text
				self.inventScreenArr.append([text, "Red"])
			else:
				self.inventScreenArr.append([text, "Yellow"])

		# Clear the side screen
		self.sideScreenArr = []
		for y in xrange(len(self.blankSideScreenArr)):
				self.sideScreenArr.append(self.blankSideScreenArr[y])
		Screen.drawSide(self.sideScreenArr)

		self.sideScreenArr = self.inventScreenArr

	## Updates the side screen to show the gear
	def updateGearScreen(self):
		self.gearScreenArr = [["GEAR", "White"],
							  ["Head:", "Yellow"],
							  [" " + self.Player.gear["Head"].name, "Yellow"],
							  ["Chest:", "Yellow"],
							  [" " + self.Player.gear["Chest"].name, "Yellow"],
							  ["Right Hand:", "Yellow"],
							  [" " + self.Player.gear["Right Hand"].name, "Yellow"],
							  ["Left Hand:", "Yellow"],
							  [" " + self.Player.gear["Left Hand"].name, "Yellow"],
							  ["Gloves:", "Yellow"],
							  [" " + self.Player.gear["Gloves"].name, "Yellow"],
							  ["Legs:", "Yellow"],
							  [" " + self.Player.gear["Legs"].name, "Yellow"],
							  ["Boots:", "Yellow"],
							  [" " + self.Player.gear["Feet"].name, "Yellow"]
							 ]

		index = self.currentGearChoice
		self.gearScreenArr[index][0] = ">" + self.gearScreenArr[index][0]
		self.gearScreenArr[index][1] = "Red"

		# Clear the side screen
		self.sideScreenArr = []
		for y in xrange(len(self.blankSideScreenArr)):
				self.sideScreenArr.append(self.blankSideScreenArr[y])

		self.sideScreenArr = self.gearScreenArr

	## Updates the side screen to show the player info
	def updatePlayerInfo(self):
		self.playerInfoScreenArr = [["PLAYER STATS", "White"],
									["", "Black"],
									["Health: " + str(self.Player.health) + "/" + str(self.Player.maxHealth), "Yellow"],
									["Attack damage: " + str(self.Player.damage), "Yellow"],
									["Defense: " + str(self.Player.defense), "Yellow"],
									["Speed: " + str(self.Player.speed), "Yellow"]
								   ]
								   #["Name: " + str(self.Player.name), "Yellow"],

		# Clear the side screen

		self.sideScreenArr = []
		for y in xrange(len(self.blankSideScreenArr)):
				self.sideScreenArr.append(self.blankSideScreenArr[y])

		self.sideScreenArr = self.playerInfoScreenArr

	## Updates the battle screen to show the currently selected choice
	def updateBattleScreen(self):
		"""
		Updates the battle screen to show the currently selected choice
		"""

		for i in xrange(2, len(self.battleScreenArr)):
			self.battleScreenArr[i][0] = " " + self.battleScreenArr[i][0][1:]
			self.battleScreenArr[i][1] = "Yellow"

		selected = self.battleScreenArr[2 + self.currentBattleChoice]
		selected[0] = ">" + selected[0][1:]
		selected[1] = "Red"

	## Returns a 2D array that is an extract of the total game array to draw to the screen
	def calculateGameScreen(self):
		"""
		Copies the correct part of the total game array to the array used for the screen

		Returns:
			An extract of totalGameArr to draw to the screen
		"""

		if(len(self.totalGameArr) < 1):
			Log("totalGameArr is less than 1. Skipping game screen calculation")
			return

		# Copy the correct part of the totalGameArray to the array used for the screen

		# Get the Y and X sizes of the screen
		sizeY = Screen.gameScreen.getmaxyx()[0] -2
		sizeX = (Screen.gameScreen.getmaxyx()[1] /2) -1

		# Get player offset positon. Floors the float created
		offsetY = int(math.floor(int(self.Player.position.y) / sizeY))
		offsetX = int(math.floor(int(self.Player.position.x) / sizeX))

		# Calculate the starting indexes of the total array to copy to the 
		startY = (offsetY) * sizeY
		startX = (offsetX) * sizeX

		endY = startY + sizeY
		endX = startX + sizeX

		for y in xrange(startY, endY):

			for x in xrange(startX, endX):

				# Try to copy the screen over from the total game array.
				# If this fails, the index is out of range and should therefore be a wall
				try:
					self.gameScreenArr[y - startY][x - startX] = self.totalGameArr[y][x]
				except IndexError as ie:
					if(self.isInDungeon):
						self.gameScreenArr[y - startY][x - startX] = [" ", "Wall"]
					else:
						self.gameScreenArr[y - startY][x - startX] = ["~", "Water"]

		# Get the player position relative to the screen
		relPlayerPosY = int(self.Player.position.y - startY)
		relPlayerPosX = int(self.Player.position.x - startX)

		# Log("Rel pos: " + str(relPlayerPosY) + " " + str(relPlayerPosX))

		self.gameScreenArr[relPlayerPosY][relPlayerPosX] = [self.Player.char, self.Player.color]


	############################################################################################################################################################

	## Spawns the player at the entrance to the floor
	def spawnPlayer(self, spawnInWorld = False):
		"""
		Spawns the player at the start or end of a floor, depending on the previous floor
		"""

		Log("Spawning player")

		if(self.currentDun == None or spawnInWorld == True):
			Screen.setTitle(self.world.name.title(), False)

			if(self.currentDun == None):
				self.Player.position = self.world.playerSpawn
				self.isInDungeon = False

			else:
				Log("Spawning at: " + str(self.currentDun.ID))
				self.Player.position = self.world.dungeonLocations[self.currentDun.ID]

			self.isInDungeon = False

		else:
			Screen.setTitle(self.currentDun.name.title(), False)

			# Check the current floor to determine where we spawn (start or end)
			if(self.currentDun.currentFloor > self.currentDun.previousFloor):

				# Get the floor's start and make it the spawn point
				spawnPos = self.currentDun.dunData[0][self.currentDun.currentFloor -1][0]
				self.Player.position = spawnPos

			else:
				# Get the floor's end and make it the spawn point
				spawnPos = self.currentDun.dunData[0][self.currentDun.currentFloor -1][1]
				self.Player.position = spawnPos

			self.isInDungeon = True

	## Checks that the player has reached the exit
	def hasReachedEnd(self):
		"""
		Checks that the player has reached the exit

		Return:
			True:			If has reached end
			False:			If not
		"""

		end = self.currentDun.dunData[0][self.currentDun.currentFloor -1][1]

		if((int(self.Player.position.x) == int(end.x)) and (int(self.Player.position.y) == int(end.y))):
			return True
		else:
			return False

	## Checks that the player has reached the start
	def hasReachedStart(self):
		"""
		Checks that the player has reached the start

		Return:
			True:			If has reached start
			False:			If not
		"""

		start = self.currentDun.dunData[0][self.currentDun.currentFloor -1][0]

		if((int(self.Player.position.x) == int(start.x)) and (int(self.Player.position.y) == int(start.y))):
			return True
		else:
			return False


	############################################################################################################################################################

	## Initialise screen
	def startScreens(self):
		"""
		Helper function to call the screen setup functions
		"""

		Log("Starting screen creation")

		Screen.createScreens()
		Screen.setTitle(self.gameTitle + " " + self.gameVersion)

		Log("Finished creating screens")

	## Shows the title screen
	def startTitleScreen(self):
		"""
		Helper function to call the title screen setup
		"""

		Log("Creating title screen")

		# Check if there is a saved game
		try:
			fileName = os.path.dirname(sys.argv[0]) + "/Data/Saves/dunList.txt"
			with open(fileName): 
				self.isSavedGame = True
		except IOError:
			self.isSavedGame = False

		Screen.showTitleScreen(self.isSavedGame)

		while(self.gameState == -1):

			# Start time
			now = time.time()
			nextTick = now + self.GAME_TICK

			self.inputLoop()

			# Sleep the remainder of the tick
			sleepyTime = nextTick - time.time()

			if(sleepyTime > 0):
				time.sleep(sleepyTime)

			else:
				Log("Ticked overtime. Not sleeping")
				pass


	############################################################################################################################################################

	## Start the game already!
	def createGame(self):
		"""
		Starts the game.

		Calls functions to initlaise everything, and then starts the game loop
		"""

		Log("Starting game creation")

		#Screen.debugBox("Starting game")

		# Start the screens
		self.startScreens()

		# Create the arrays, based on the sizes of each screen

		Log("Getting dimensions of gameScreen")

		# Game screen
		screenSizeY = Screen.gameScreen.getmaxyx()[0] - 2				# Subtract 2 for the edges of the screen
		screenSizeX = ((Screen.gameScreen.getmaxyx()[1] - 2) / 2)		# Subtract 2 for the edges of the screen. Halved because the terminal has double spaced line spacing

		Log("Dimensions (y, x):")
		Log(str(screenSizeY) + " "  + str(screenSizeX))

		for y in xrange(screenSizeY):
			self.gameScreenArr.append([])
			for x in xrange(screenSizeX):
				self.gameScreenArr[y].append([" ", "Black"])

		Log("Getting dimensions of sideScreen")

		# Side screen
		screenSizeX = Screen.sideScreen.getmaxyx()[1] -2				# Subtract 2 for the edges of the screen
		screenSizeX = Screen.sideScreen.getmaxyx()[1] -2				# Subtract 2 for the edges of the screen

		Log("Dimensions (y, x):")
		Log(str(screenSizeY) + " "  + str(screenSizeX))

		for y in xrange(screenSizeY):
			self.sideScreenArr.append([" " * screenSizeX, "Black"])
			self.blankSideScreenArr.append([" " * screenSizeX, "Black"])

		Log("Getting dimensions of lowerScreen")

		# Lower screen
		screenSizeY = Screen.lowerScreen.getmaxyx()[0] - 2				# Subtract 2 for the edges of the screen
		screenSizeX = Screen.lowerScreen.getmaxyx()[1] - 2				# Subtract 2 for the edges of the screen

		Log("Dimensions (Y, X):")
		Log(str(screenSizeY) + " "  + str(screenSizeX))

		for y in xrange(screenSizeY):
			self.lowerScreenArr.append([])
			for x in xrange(screenSizeX):
				self.lowerScreenArr[y].append([" ", "Black"])

		Screen.debugBox("Game set up successfully")


		Log("Setting game state to 0")

		self.gameState = 0

		Log("Finished game creation")

	## Ends the game. Kills all screens and the curses module
	def endGame(self, playerHasDied = False):
		"""
		Ends the game.

		Calls functions to properly end everything

		Parameters:
			- playerHasDied:			If the player has died
		"""

		self.isPlaying = False

		# Delete the save file if the player has died
		if(playerHasDied):
			try:
				saveDir = os.path.dirname(sys.argv[0]) + "/Data/Saves"
				shutil.rmtree(saveDir)
				os.mkdir(saveDir)

				mapsDir = os.path.dirname(sys.argv[0]) + "/Data/Maps"
				shutil.rmtree(mapsDir)
				os.mkdir(mapsDir)

			except:
				pass

		else:
			# Save game
			self.saveGame()

		Screen.debugBox("Ending game")
		Screen.endScreens()
		curses.echo()
		curses.curs_set(1)

		Screen.debugBox("Game ended successfully")

		# Kill the terminal
		sys.exit()


	############################################################################################################################################################

	## Starts a new game
	def startNewGame(self):
		"""
		Starts a new game.

		This is a function with subfunctions to generate the player, world and dungeons.
		Each dungeon has a name and and ID. The ID is the "level" the dungeon represents, the name is it's name
		"""

		## Generates a new player
		def __generatePlayer():
			"""
			Geneartes a new player
			"""

			Log("Creating player")

			# Create a new player
			self.Player = gamePlayer()

			Log("Adding items to player")

			# Add the default items to the player
			item = GetItem("WoodenSword")
			self.Player.addToGear(item, "Right Hand")

			item = GetItem("WoodenShield")
			self.Player.addToGear(item, "Left Hand")

			item = GetItem("NoHead")
			self.Player.addToGear(item, "Head")

			item = GetItem("NoChest")
			self.Player.addToGear(item, "Chest")

			item = GetItem("NoLegs")
			self.Player.addToGear(item, "Legs")

			item = GetItem("NoBoots")
			self.Player.addToGear(item, "Feet")

			item = GetItem("NoGloves")
			self.Player.addToGear(item, "Gloves")

			item = GetItem("LeatherGloves")
			self.Player.addToInventory(item)

			# Add some consumables
			item = GetItem("Steak")
			self.Player.addToInventory(item)
			self.Player.addToInventory(item)
			self.Player.addToInventory(item)

			# Add 3 random items
			item = GetAnyItem()
			self.Player.addToInventory(item)
			item = GetAnyItem()
			self.Player.addToInventory(item)
			item = GetAnyItem()
			self.Player.addToInventory(item)


			Log("Items added, saving player")
			SavePlayer(self.Player)

		## Generates a new dungeon
		def __generateDungeon(name, ID):
			"""
			Generates a new dungeon
			"""

			msg = "Generating dungeon %i..." %(ID)

			Screen.writeLower([msg, "Magenta"]) 
			self.updateScreen()

			newDun = Dungeon(1, 100, 100, 3, ID)
			newDun.Generate()

			Screen.writeLower(["Generation complete. Saving dungeon", "Magenta"])
			self.updateScreen()

			newDun.Save()

			Screen.writeLower(["Save complete", "Magenta"])
			self.updateScreen()

			Screen.writeLower(["Dungeon generation done", "Magenta"])

			self.dungeonList.append(newDun.name)

		Log("Starting new game")

		## Generates the world
		def __generateWorld(name):
			"""
			Generates a new world
			"""
			self.world = World(100, 100, seed = 0)
			self.world.Generate()

			Screen.writeLower(["Generating world", "Magenta"])
			self.updateScreen()

			for i in xrange(5):
				location = self.world.GetDungeonLocation()

				self.world.dungeonLocations.append(location)

				self.currentDun = LoadDungeon(self.dungeonList[i])
				self.currentDun.location = location

				Log("Spawn location for " + self.currentDun.name.title() + ": " + str(self.currentDun.location))

				self.currentDun.Save()
			self.currentDun = None

			self.world.AddDungeons()

			SaveWorld(self.world)

		# Delete the previous game. This folder may not exist
		try:
			saveDir = os.path.dirname(sys.argv[0]) + "/Data/Saves"
			shutil.rmtree(saveDir)
			os.mkdir(saveDir)

			mapsDir = os.path.dirname(sys.argv[0]) + "/Data/Maps"
			shutil.rmtree(mapsDir)
			os.mkdir(mapsDir)

		except:
			pass

		# Geneate player
		__generatePlayer()

		# Generate the dungeons
		for i in xrange(5):
			name = "Dun" + str(i)
			__generateDungeon(name, i)

		# Generate the world
		__generateWorld("LOLOLOLOLOLOLOPOLIS")

		Log(self.dungeonList[0])

		self.Player.currentDungeon = -1
		self.totalGameArr = self.world.worldData

		self.spawnPlayer(True)
		self.recalcScreen = True
		self.isPlaying = True

		self.saveGame()

	## Continue a game
	def continueGame(self):
		"""
		Continues a game from file
		"""

		Log("Continuing game")

		filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/dunList.txt"
		with open(filePath, "r") as saveFile:
			self.dungeonList = saveFile.read().split("\n")

		Screen.writeLower(["Loading game...", "Magenta"])

		self.world = LoadWorld()

		self.Player.currentDungeon = -1
		self.totalGameArr = self.world.worldData

		self.isInDungeon = False

		self.spawnPlayer(True)
		self.recalcScreen = True
		self.isPlaying = True

		self.saveGame()

	## Loads player from file
	def loadPlayer(self):
		self.Player = LoadPlayer()

	## Loads a dungeon from file
	def loadDungeon(self, dunName):
		"""
		Loads a dungeon from file
		"""

		Log("Loading dungeon")

		Screen.writeLower(["Loading dungeon. This might take some time", "Magenta"])
		self.updateScreen()

		self.currentDun = LoadDungeon(dunName)
		self.Player.currentDungeon = self.currentDun.ID

		Screen.writeLower(["Load complete", "Magenta"])
		self.updateScreen()

		self.totalGameArr = self.currentDun.getCurrentFloor()

		self.spawnPlayer()
		self.recalcScreen = True

		Log("Load complete")

	## Saves a game
	def saveGame(self, silent = False):

		# Print a message if not silent
		if(not silent):
			Screen.writeLower(["Saving game", "Magenta"])

		# Save the player
		SavePlayer(self.Player)

		# Save dungeon list
		filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/dunList.txt"

		with open(filePath, "w") as saveFile:
			for dunName in self.dungeonList:
				saveFile.write(dunName)
				saveFile.write("\n")

	############################################################################################################################################################

	## Spawns entities
	def spawnEntities(self):
		"""
		Stub to spawn entities

		NOTE: Not used in game

		"""

		sizeX = self.currentDun.sizeX -1
		sizeY = self.currentDun.sizeY -1

		Screen.debugBox("Starting entity spawning")

		i = 0
		while i < 10:

			posX = random.randint(0, sizeX)
			posY = random.randint(0, sizeY)

			if(self.totalGameArr[posY][posX][1] == "Floor"):

				Screen.debugBox("Spawning entity " + str(i))

				mon = Zombie(position = Vector2(posX, posY))
				self.allMonsters.append(mon)
				i += 1

		Screen.debugBox("Finished entity spawning")

	## Entity move logic
	def moveEntities(self):
		"""
		Gets a flag from the entity if it can move this tick. 
		If so, check the area around this entity, and move to a valid tile.

		If the player is near the entity, move towards it

		NOTE: Not used in game

		"""

		choices = [Vector2(0, 1), Vector2(0, 1), Vector2(0, 1), Vector2(0, -1), Vector2(1, 0), Vector2(-1, 0)]

		for mon in self.allMonsters:
			if(mon.canMove(self.CURRENT_GAME_TICK)):
				
				direction = random.choice(choices)

				if(self.totalGameArr[int(mon.position.y + direction.y)][int(mon.position.x + direction.x)][1]) == "Floor":
					mon.Move(direction)

	############################################################################################################################################################

	## Rolls a random to determine if a battle will take place
	def rollBattle(self):
		"""
		Rolls a random to determine if a battle will take place
		"""

		if(self.isInDungeon):

			rate = self.currentDun.getSpawnRate()
			# Reset the seed to a random value (actually the system time)
			random.seed()
			roll = random.uniform(0, 1)

			if (roll <= rate):
				self.startBattle()

	## Called when a battle needs to be started
	def startBattle(self):
		"""
		Starts a battle by spawning the entity and changing the game state
		"""
		#curses.flash()
		curses.beep()
		curses.beep()

		# Update game state
		self.gameState = 1

		# Randomise the random seed
		random.seed()

		# Roll to see if a normal monster or the rare itemDropper is spawned
		if(random.uniform(0, 1) <= 0.01):
			mon = ItemDropper()

		else:
			# Generate a new monster
			monType = random.choice(self.currentDun.monsterList)
			# Python allows a string to be parsed as code, using the eval(string) function
			evalString = monType + "()"
			mon = eval(evalString)

		# Tweak the monster's attributes based on the player's attributes
		mon.damage *= 1 + (0.25 * self.Player.defense)
		mon.defense *= 1 + (0.25 * self.Player.damage)

		self.allMonsters.append(mon)

		# Clear the side screen
		self.sideScreenArr = []
		for y in xrange(len(self.blankSideScreenArr)):
				self.sideScreenArr.append(self.blankSideScreenArr[y])

		Screen.sideScreen.clear()
		
		# Set up game screen
		self.battleScreenArr = [["OPTIONS:", "White"],
								["", "Black"],
								["  Attack", "Yellow"],
								["  Flee", "Yellow"],
							   ]

		Screen.sideScreen.border("|", "|", "-", "-")

		for i in xrange(len(self.battleScreenArr)):

			self.sideScreenArr[i] = self.battleScreenArr[i]

		msg = "A " + mon.name + " appeared!"
		Screen.writeLower([msg, "White"])

		healthMsg = "%s health: %i / %i \t\t %s health: %i / %i" %(self.Player.name, self.Player.health, self.Player.maxHealth, mon.name, mon.health, mon.maxHealth)

		Screen.writeLower([" ", "Black"])
		Screen.writeLower([" ", "Black"])
		Screen.writeLower([healthMsg, "Yellow"])

	## Does one battel turn, where both the player and the enemy attack
	def doBattleTurn(self):
		"""
		Does one battle turn. Most battle logic is done here, except for starting and ending a battle.

		Subfunctions:
			- showHealthMsg:			Displays the current health for the player and the monster
				- Returns:					The current health of the player and the monster
			- showTurn:					Displays the results from the current turn
				- Parameters:
					- turn1:				The first turn's result
					- turn2:				The second turn's result
					- mon:					The monster in battle
				- Returns:					The results of this complete battle turn
		"""

		# Sub function to show the health message at the bottom part of the game log
		def showHealthMsg():
			healthMsg = "%s health: %i / %i \t\t %s health: %i / %i" %(self.Player.name, self.Player.health, self.Player.maxHealth, mon.name, mon.health, mon.maxHealth)
			#Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([healthMsg, "Yellow"])

		# Sub function to show the results of the turn
		def showTurn(turn1, turn2, mon):

			# Clear screen
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])
			self.updateScreen()

			# Write turn 1
			Screen.writeLower(turn1)
			self.updateScreen()
			time.sleep(self.GAME_TICK * 15)

			# Write turn 2
			Screen.writeLower(turn2)
			self.updateScreen()
			time.sleep(self.GAME_TICK * 15)

			# Display health, only if both the player and monster are still alive
			if(mon.health > 0 and self.Player.health > 0):
				showHealthMsg()
				self.updateScreen()
			
		# Get the new monster
		mon = self.allMonsters[0]

		# Get attack damage values from the player and the monster
		playerDmg = int(self.Player.Attack())
		monDmg = int(mon.Attack())

		# Critical hit flags
		playerCrit = False
		monCrit = False

		# Roll for critical hits
		if(random.random() < 0.1):
			playerDmg *= 3
			playerCrit = True

		if(random.random() < 0.1):
			monDmg *= 3
			monCrit = True

		# Flag for if the monster did do damage when they were not meant to have
		monDidGhostDamage = False

		# Display text and colorPair name
		msg = "BATTLE TEXT"
		col = "White"


		# Higher speed -> attack first

		# Player has higher speed
		if(self.Player.speed >= mon.speed):
			
			# Deal damage player -> monster
			if(playerDmg == False):
				msg = "%s avoided the attack!" %(mon.name)
				col = "Red"
				
			else:
				result = mon.GetHit(playerDmg)
				msg = "%s took %i damage" %(mon.name, result)

				if(playerCrit):
					msg = "CRITCAL HIT!!! " + msg

				col = "Green"

			# Set turn1 results
			turn1 = [msg, col]

			# Deal damage monster -> player
			if(monDmg == False):
				msg = "%s avoided the attack!" %(self.Player.name)
				col = "Green"
				
			else:

				if(mon.health > 0):
					result = self.Player.GetHit(monDmg)
					msg = "%s took %i damage" %(self.Player.name, result)

					if(monCrit):
						msg = "CRITCAL HIT!!! " + msg

					col = "Red"

				else:
					msg = "%s has no health!" %(mon.name)
					col = "Green"

			# Set turn2 results
			turn2 = [msg, col]

			# Display this full battle turn
			showTurn(turn1, turn2, mon)

		# Monster has higher speed
		else:
			# Deal damage monster -> player
			if(monDmg == False):
				msg = "%s avoided the attack!" %(self.Player.name)
				col = "Green"
				
			else:
				result = self.Player.GetHit(monDmg)
				msg = "%s took %i damage" %(self.Player.name, result)

				if(monCrit):
					msg = "CRITCAL HIT!!! " + msg

				col = "Red"
					
			# Set turn1 results
			turn1 = [msg, col]

			# Deal damage player -> monster
			if(playerDmg == False):
				msg = "%s avoided the attack!" %(mon.name)
				col = "Red"
				
			else:
				# If the player is alive, they can do damage
				if(self.Player.health > 0):
					result = mon.GetHit(playerDmg)
					msg = "%s took %i damage" %(mon.name, result)

					if(playerCrit):
						msg = "CRITCAL HIT!!! " + msg

					col = "Green"

				else:
					msg = "%s has no health!" %(self.Player.name)
					col = "Red"

			# Set turn2 results
			turn2 = [msg, col]

			# Display this full battle turn
			showTurn(turn1, turn2, mon)

		# Check to see if the battle is over

		# Check if player is slain
		if(self.Player.health <= 0):
			
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])
			Screen.writeLower([" ", "Black"])

			msg =  "%s has been defeated" %(self.Player.name)
			Screen.writeLower([msg, "Red"])

			# Update screen and sleep
			self.updateScreen()
			time.sleep(self.GAME_TICK * 10)

			self.endBattle()
			self.endGame(True)

			return

		# Check if monster is slain
		if(mon.health <= 0):
			msg = mon.name + " has been slain"
			Screen.writeLower([msg, "Green"])

			self.endBattle()

			return

	## Does one battle turn, where only the enemy attacks
	def doBattleTurnNoAttack(self):
		"""
		Does one battle turn, without the player attacking.

		NOTE: Not used

		"""
		Screen.writeLower(["Battle turn no attack", "White"])

	## Ends the current battle
	def endBattle(self, didRun = False):
		"""
		Ends the current battle
		"""
		
		mon = self.allMonsters[0]

		# Only drop items if the player killed the monster
		if(not didRun):
			# If the monster is an Item Dropper, always drop an item
			if(type(mon) == ItemDropper):

				item = GetAnyItem()

				Screen.writeLower(["Rare item drop!", "Yellow"])
				msg = "%s received" %(item.name)
				Screen.writeLower([msg, "Green"])

				self.Player.addToInventory(item)

			else:
				# Drop an item if need be
				random.seed()
				if(random.uniform(0, 1) < 0.50):		# Half chance to drop an item

					# Roll to choose either a normal drop or a rare drop. A rare drop can be any item in the game
					if(random.uniform(0, 1) < 0.05):
						item = GetAnyItem()
						Screen.writeLower(["Rare item drop!", "Yellow"])

					else:
						item = GetItem(random.choice(mon.drops))

					self.Player.addToInventory(item)

					msg = "%s received" %(item.name)
					Screen.writeLower([msg, "Green"])

		# Kill the monster
		mon.Die()
		self.allMonsters.pop()

		# Clear the side screen
		Screen.sideScreen.clear()
		Screen.sideScreen.border("|", "|", "-", "-")

		self.sideScreenArr = []
		for y in xrange(len(self.blankSideScreenArr)):
				self.sideScreenArr.append(self.blankSideScreenArr[y])

		# Print the coward's message
		if(didRun):
			Screen.writeLower(["You fled from battle", "Green"])
			Screen.writeLower([" ", "Black"])

		Screen.writeLower([" ", "Black"])
		Screen.writeLower([" ", "Black"])

		self.updateScreen()

		# Return to the main gameState
		self.gameState = 0

		# Delete the monster to free up memory
		del(mon)

	############################################################################################################################################################

	## Action button. Called whenever the action button is pressed (z)
	def action(self, option = 0):
		"""
		Called whenever the action button is pressed.

		Examines the current state of the game, as well as where the player is, and then determines an action.
		For example:
			- The player is at the exit of a dungeon and presses the button. They will leave the dungeon
			- The player is in battle and has "attack" selected. They will attack

		Parameters:
			- option:			An optional integer to determine a choice
		"""

		# Check the state of the game and do actions specific to the state
		if(self.gameState == 0):					# In overworld / dungeon

			# If in a dungeon
			if(self.isInDungeon):

				floor = self.currentDun.getCurrentFloor()
				tileData = floor[int(self.Player.position.y)][int(self.Player.position.x)]
				tileChar = tileData[0]
				tileTag = tileData[1]

				if(tileTag == "End"):
					if(self.currentDun.setCurrentFloor(self.currentDun.currentFloor + 1)):
						
						Screen.writeLower(["Going down...", "Green"])

						self.totalGameArr = self.currentDun.getCurrentFloor()
						self.spawnPlayer()
					else:
						# If we're at the end, leave this dungeon
						Screen.writeLower(["Escaping to the surface...", "Blue"])
						self.totalGameArr = self.world.worldData
						self.spawnPlayer(True)


				elif(tileTag == "Start"):

					if(self.currentDun.setCurrentFloor(self.currentDun.currentFloor - 1)):
						
						Screen.writeLower(["Going up...", "Green"])
						self.totalGameArr = self.currentDun.getCurrentFloor()
						self.spawnPlayer()

					else:
						# If we're at the start, leave this dungeon
						Screen.writeLower(["Escaping to the surface...", "Blue"])
						self.totalGameArr = self.world.worldData
						self.spawnPlayer(True)

			# If in overworld
			elif(not self.isInDungeon):
				playerPos = self.Player.position

				i = 0
				# Check if we can go into a dungeon
				for pos in self.world.dungeonLocations:
					if(pos == playerPos):

						self.currentDun = LoadDungeon(self.dungeonList[i])

						msg = "Entering %s..." %(self.currentDun.name.title())
						Screen.writeLower([msg, "Blue"])

						self.Player.currentDungeon = i
						self.totalGameArr = self.currentDun.getCurrentFloor()
						self.spawnPlayer()
						return 						# Stop the loop

					i +=1

				# If no action, beep
				curses.beep()


			# If no action, beep
			else:
				curses.beep()

		elif(self.gameState == 1):					# In battle

			# Attack
			if(option == 0):
				self.doBattleTurn()

			# Inventory - NOT USED IN CURRENT BUILD
			#elif(option == 1):
			#	pass

			# Gear - NOT USED IN CURRENT BUILD
			#elif(option == 2):
			#	pass

			# Run
			elif(option == 1):
				self.endBattle(True)

		elif(self.gameState == 2):					# In inventory - Not implemented here, all action is done in the input loop
			pass

		elif(self.gameState == 3):					# In gear - Not implemented here, all action is done in the input loop
			pass

	############################################################################################################################################################

	## Input loop. Called once per tick
	def inputLoop(self):
		"""
		Input loop. Called once per tick
		"""

		key = Screen.getPlayerInput()

		# If we are playing
		if(self.isPlaying):

			# ENDS GAME
			if(key == 27):	# ESC was pressed
				self.isPlaying = False
				self.endGame()
				return True

			# Check the current state. No case selection, so hacky elif time

			###############
			#			  #
			# NOT STARTED #						# Should never hit this when the game is playing
			#			  #
			###############

			if(self.gameState == -1):
				pass


			##########################
			#			  			 #
			# IN OVERWORLD / DUNGEON #
			#			  			 #
			##########################

			elif(self.gameState == 0):

				# Movement
				if(key == curses.KEY_UP):
					if(self.totalGameArr[int(self.Player.position.y -1)][int(self.Player.position.x)][1] != "Wall" and self.totalGameArr[int(self.Player.position.y -1)][int(self.Player.position.x)][1] != "Water"):
						self.Player.Move(Vector2(0, -1))
						self.rollBattle()
						self.recalcScreen = True

				elif(key == curses.KEY_DOWN):
					if(self.totalGameArr[int(self.Player.position.y +1)][int(self.Player.position.x)][1] != "Wall" and self.totalGameArr[int(self.Player.position.y +1)][int(self.Player.position.x)][1] != "Water"):
						self.Player.Move(Vector2(0, 1))
						self.rollBattle()
						self.recalcScreen = True

				elif(key == curses.KEY_LEFT):
					if(self.totalGameArr[int(self.Player.position.y)][int(self.Player.position.x -1)][1] != "Wall" and self.totalGameArr[int(self.Player.position.y)][int(self.Player.position.x -1)][1] != "Water"):
						self.Player.Move(Vector2(-1, 0))
						self.rollBattle()
						self.recalcScreen = True

				elif(key == curses.KEY_RIGHT):
					if(self.totalGameArr[int(self.Player.position.y)][int(self.Player.position.x +1)][1] != "Wall" and self.totalGameArr[int(self.Player.position.y)][int(self.Player.position.x +1)][1] != "Water"):
						self.Player.Move(Vector2(1, 0))
						self.rollBattle()
						self.recalcScreen = True

				# z (action key)
				if(key == ord("z") or key == ord("Z")):
					self.action()
					

				# To the inventory screen
				if(key == ord("x") or key == ord("X")):

					self.gameState = 2
					Screen.sideScreen.clear()
					Screen.sideScreen.border("|", "|", "-", "-")

				# To the gear screen
				if(key == ord("c") or key == ord("C")):

					self.gameState = 3
					Screen.sideScreen.clear()
					Screen.sideScreen.border("|", "|", "-", "-")

			#############
			# 			#
			# IN BATTLE #
			#			#
			#############

			elif(self.gameState == 1):

				# Move selection
				if(key == curses.KEY_UP):
					self.currentBattleChoice -= 1

					if(self.currentBattleChoice <= 0):
						self.currentBattleChoice = 0

				elif(key == curses.KEY_DOWN):
					self.currentBattleChoice += 1

					if(self.currentBattleChoice >= 1):
						self.currentBattleChoice = 1

				elif(key == curses.KEY_LEFT):
					pass

				elif(key == curses.KEY_RIGHT):
					pass

				# z (action key)
				if(key == ord("z") or key == ord("Z")):
					self.action(self.currentBattleChoice)


			################
			#			   #
			# IN INVENTORY #
			#			   #
			################

			elif(self.gameState == 2):

				# Return focus to game screen
				if(key == ord("x") or key == ord("X")):
					self.gameState = 0

					# Clear the side screen

					Screen.sideScreen.clear()
					Screen.sideScreen.border("|", "|", "-", "-")

					self.sideScreenArr = []
					for y in xrange(len(self.blankSideScreenArr)):
							self.sideScreenArr.append(self.blankSideScreenArr[y])

				# Use the current item
				if(key == ord("z") or key == ord("Z")):

					msg = "Used %s" %(self.Player.inventory[self.currentInventoryChoice])
					Screen.writeLower([msg, "Green"])

					self.Player.useItem(self.currentInventoryChoice)
					self.updatePlayerInfo()

				# Delete the current item from inventory
				if(key == ord("w") or key == ord("W")):

					if(len(self.Player.inventory) > 0):
						msg = "Removed %s from inventory" %(self.Player.inventory[self.currentInventoryChoice])

						Screen.writeLower([msg, "Red"])
						self.Player.removeFromInventory(self.currentInventoryChoice)



				# Move selection
				if(key == curses.KEY_UP):
					self.currentInventoryChoice -= 1

					if(self.currentInventoryChoice <= 0):
						self.currentInventoryChoice = 0

				elif(key == curses.KEY_DOWN):
					self.currentInventoryChoice += 1

					if(self.currentInventoryChoice >= len(self.Player.inventory) -1):
						self.currentInventoryChoice = len(self.Player.inventory) -1


			###########
			#		  #
			# IN GEAR #
			#		  #
			###########

			elif(self.gameState == 3):

				# Return focus to game screen
				if(key == ord("x") or key == ord("X")):

					Screen.sideScreen.clear()
					Screen.sideScreen.border("|", "|", "-", "-")

					# Clear the side screen
					self.sideScreenArr = []
					for y in xrange(len(self.blankSideScreenArr)):
							self.sideScreenArr.append(self.blankSideScreenArr[y])

					self.gameState = 0

				# TODO: ON Z DOWN

				# Move selection
				if(key == curses.KEY_UP):
					self.currentGearChoice -= 2

					if(self.currentGearChoice <= 2):
						self.currentGearChoice = 2

				elif(key == curses.KEY_DOWN):
					self.currentGearChoice += 2

					if(self.currentGearChoice >= 14):
						self.currentGearChoice = 14

		else:

			# If the game state is -1 (not started, listen for some inputs)
			if(self.gameState == -1):

				if(key == ord("z") or key == ord("Z")):
					Screen.endTitleScreen()
					self.createGame()
					#self.isPlaying = True
					self.startNewGame()
					self.mainLoop()
					
				if((key == ord("c") and self.isSavedGame) or (key == ord("Z") and self.isSavedGame)):
					Screen.endTitleScreen()
					self.createGame()
					self.loadPlayer()
					#self.isPlaying = True
					self.continueGame()
					self.mainLoop()

		# Flushes the input so that no more input is processed
		curses.flushinp()



	############################################################################################################################################################

	## Main game loop
	def mainLoop(self):
		"""
		Main loop of the game, executed every tick, only after the game has been set up
		"""

		while(True):

			nextTick = time.time() + self.GAME_TICK

			self.CURRENT_GAME_TICK += 1
			Log("Tick " + str(self.CURRENT_GAME_TICK))

			##########################

			# Stop the game if the input loop returns "True"
			if(self.inputLoop()):
				break

			# isPlaying logic
			if(self.isPlaying):
				#self.moveEntities()
				pass

			##########################

			# Save the game every 100 ticks (~every 10 seconds)
			if(self.CURRENT_GAME_TICK % 100 == 0):
				self.saveGame(True)

			##########################

			# Update the screen
			self.updateScreen()

			##########################

			# Sleep the remainder of the tick
			sleepyTime = nextTick - time.time()

			if(sleepyTime > 0):
				time.sleep(sleepyTime)

			else:
				Log("Ticked overtime. Not sleeping")
				pass
			
	############################################################################################################################################################

# Start the game if this file is ran, NOT imported
if __name__ == '__main__':

	game = Game()
	Screen.createInitialColors()
	game.startTitleScreen()
	
	

	



