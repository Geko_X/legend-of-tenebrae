# Colors.py, written by X.Hunt
#########

"""
This handles all color methods, 
as well as holding a dictinary of {name, color pair ID} and a list of [color pair names].

A color pair is a pair of colors used by curses to add color to the screen.
A pair is defined as an:
	- id (starting at 1 as 0 is default white on black)
	- foreground (one of 8 possible colors)
	- background (one of 8 possible colors)

Color pairs are defined, and then added to a dictionary.
The dictionary is defined as:
	Key: String
	Value: Integer

The value is the same as the id for that particular color pair.

Each pairID is keyed by it's name, 
	eg: "Water" is a color pair with blue foreground and cyan background.
		Colors["Water"] => 2

Color codes:
	0 - Black
	1 - Red
	2 - Green
	3 - Yellow
	4 - Blue
	5 - Magenta
	6 - Cyan
	7 - White	
"""

# Imports
import curses as c
screen = c.initscr()
c.start_color()

# Variables

## Global color dictionary.
## This is used to loook up a calor pair from a name
colors = {}

## Global color name list.
## This is a list containing all the names of color pairs
colorList = []

# Functions

def getCursesColorPair():
	"""
	Returns the curses color pair list

	Return:
		- c.color_pair()
	"""
	return c.color_pair()

def getColorPair(name):
	"""
	Returns the color pair ID with the given name

	Parameters:
		- name:			The name of the color pair

	Return:
		- Integer representing the given color pair name
	"""
	return colors[name]


def addColorPair(name, foreground, background):
	"""
	Adds a color pair to the dictionary of colors

	Parameters:
		- name:			The name of the color pair
		- foreground:	The color code for the foreground
		- background:	The color code for the background
	"""

	## The ID of the color pair
	pairID = len(colors)

	# Create the color pair
	curses.init_pair(pairID, foreground, background)

	# Add the color pair to the dictionary and list
	colors[name] = pairID
	colorList.append(name)


def getColorList():
	"""
	Returns a list of all color pair names

	Return: 
		- List containing all color pair names
	"""
	return colorList

def createInitialColors():
	"""
	Creates the initial color pairs
	"""

	addColorPair("Floor", 1, 0)		# Red on black
	addColorPair("Water", 6, 8)		# Cyan on blue
	addColorPair("Wall", 3, 0)		# Yellow on black





