#########################################
#										#
# Consumable classes, written by X.Hunt #
#										#
#########################################

from Items import Consumable
from .Classes.Log import log as Log
from .Classes import Screen

class HealthRestore(Consumable):
	"""
	HealthRegen base class. 
	This extends the Consumable class, and is extended by "specialised" HealthRegen types.

	Eg: potions, food...
	"""
	def __init__(self, **kwargs):
		"""
		Class constructor
		"""
		Consumable.__init__(self, **kwargs)

	## Restores health to the player
	def Use(self, Player, index):
		"""
		Restores health to the given player

		Parameters:
			- Player:			The player to heal
			- index:		The index of this item
		"""
		Player.Heal(self.healAmount)
		Player.removeFromInventory(index)
		Player.updateGear()

		# Inform the player
		msg = "Healed by %i" %(self.healAmount)
		Screen.writeLower([msg, "Green"])

class Food(HealthRestore):
	def __init__(self,
			 itemName = "Food",
			 itemType = "HealthRestore",
			 healAmount = 10,
			 damageIncrease = 0,
			 healthIncrease = 0,
			 defenseIncrease = 0,
			 speedIncrease = 0):

		kwargs = {"itemName" : itemName,
				  "itemType" : itemType,
				  "healAmount" : healAmount,
				  "damageIncrease" : damageIncrease,
				  "healthIncrease" : healthIncrease,
				  "defenseIncrease" : defenseIncrease,
				  "speedIncrease" : speedIncrease}

		HealthRestore.__init__(self, **kwargs)

class HealthPotion(HealthRestore):
	def __init__(self,
			 itemName = "HealthPotion",
			 itemType = "HealthRestore",
			 healAmount = 50,
			 damageIncrease = 0,
			 healthIncrease = 0,
			 defenseIncrease = 0,
			 speedIncrease = 0):

		kwargs = {"itemName" : itemName,
				  "itemType" : itemType,
				  "healAmount" : healAmount,
				  "damageIncrease" : damageIncrease,
				  "healthIncrease" : healthIncrease,
				  "defenseIncrease" : defenseIncrease,
				  "speedIncrease" : speedIncrease}

		HealthRestore.__init__(self, **kwargs)

class RestorationScroll(HealthRestore):
	def __init__(self,
			 itemName = "RestorationScroll",
			 itemType = "HealthRestore",
			 healAmount = 100,
			 damageIncrease = 0,
			 healthIncrease = 0,
			 defenseIncrease = 0,
			 speedIncrease = 0):

		kwargs = {"itemName" : itemName,
				  "itemType" : itemType,
				  "healAmount" : healAmount,
				  "damageIncrease" : damageIncrease,
				  "healthIncrease" : healthIncrease,
				  "defenseIncrease" : defenseIncrease,
				  "speedIncrease" : speedIncrease}

		HealthRestore.__init__(self, **kwargs)

#######################################################################################

class StatIncrease(Consumable):
	"""
	Stat increasing base class

	This extends the Consumable class, and is extended by "specialised" increasing types

	Eg: Potion, scrolls...
	"""

	def __init__(self, **kwargs):
		"""
		Class constructor
		"""
		Consumable.__init__(self, **kwargs)

	## Increase the player's base stats
	def Use(self, Player, index):
		"""
		Increases the given player's base stats

		Parameters:
			- Player:			The player to increase stats
			- index:			The index of this item
		"""

		Player.baseMaxHealth += self.healthIncrease
		Player.baseDamage += self.damageIncrease
		Player.baseDefense += self.defenseIncrease
		Player.baseSpeed += self.speedIncrease

		# Inform the player if their stats have been changed
		if(self.healthIncrease != 0):
			if(self.healthIncrease > 0):
				msg = "Health increased by %i" %(self.healthIncrease)
				Screen.writeLower([msg, "Green"])
			if(self.healthIncrease < 0):
				msg = "Health decreased by %i" %(self.healthIncrease)
				Screen.writeLower([msg, "Red"])

		if(self.damageIncrease != 0):
			if(self.damageIncrease > 0):
				msg = "Damage increased by %i" %(self.damageIncrease)
				Screen.writeLower([msg, "Green"])
			if(self.damageIncrease < 0):
				msg = "Damage decreased by %i" %(self.damageIncrease)
				Screen.writeLower([msg, "Red"])

		if(self.defenseIncrease != 0):
			if(self.defenseIncrease > 0):
				msg = "Defense increased by %i" %(self.defenseIncrease)
				Screen.writeLower([msg, "Green"])
			if(self.defenseIncrease < 0):
				msg = "Defense decreased by %i" %(self.defenseIncrease)
				Screen.writeLower([msg, "Red"])

		if(self.speedIncrease != 0):
			if(self.speedIncrease > 0):
				msg = "Speed increased by %i" %(self.speedIncrease)
				Screen.writeLower([msg, "Green"])
			if(self.speedIncrease < 0):
				msg = "Speed decreased by %i" %(self.speedIncrease)
				Screen.writeLower([msg, "Red"])

		Player.removeFromInventory(index)
		Player.updateGear()

class IncreasingScroll(StatIncrease):
	def __init__(self,
			 itemName = "Scroll",
			 itemType = "StatIncrease",
			 healAmount = 0,
			 damageIncrease = 1,
			 healthIncrease = 1,
			 defenseIncrease = 1,
			 speedIncrease = 1):

		kwargs = {"itemName" : itemName,
				  "itemType" : itemType,
				  "healAmount" : healAmount,
				  "damageIncrease" : damageIncrease,
				  "healthIncrease" : healthIncrease,
				  "defenseIncrease" : defenseIncrease,
				  "speedIncrease" : speedIncrease}

		StatIncrease.__init__(self, **kwargs)

# class IncreasingPotion(StatIncrease):
# 	def __init__(self,
# 			 itemName = "Potion",
# 			 itemType = "StatIncrease",
# 			 healAmount = 0,
# 			 damageIncrease = 1,
# 			 healthIncrease = 1,
# 			 defenseIncrease = 1,
# 			 speedIncrease = 1):

# 		kwargs = {"itemName" : itemName,
# 				  "itemType" : itemType,
# 				  "healAmount" : healAmount,
# 				  "damageIncrease" : damageIncrease,
# 				  "healthIncrease" : healthIncrease,
# 				  "defenseIncrease" : defenseIncrease,
# 				  "speedIncrease" : speedIncrease}

# 		StatIncrease.__init__(self, **kwargs)

#######################################################################################
#######################################################################################
#################### NOT USED ----- CUT FROM GAME ----- NOT USED #######W##############
#######################################################################################
#######################################################################################

# class Enchanting(Consumable):
# 	"""
# 	Enchanting base class

# 	This extends the Consumable class, and is extended by "specialised" enchanting types

# 	Eg: Potion, scrolls...
# 	"""

# 	def __init__(self, **kwargs):
# 		"""
# 		Class constructor
# 		"""
# 		Consumable.__init__(self, **kwargs)

# 	## Increase the player's base stats
# 	def Use(self, Player, slot):
# 		"""
# 		Increases the given player's base stats for a particular gear

# 		Parameters:
# 			- Player:			The player to increase stats
# 			- slot:				The gear slot to enchant to
# 		"""
# 		Player.gear[slot].healthBonus += self.healthIncrease
# 		Player.gear[slot].damageBonuse += self.damageIncrease
# 		Player.gear[slot].defenseBonus += self.defenseIncrease
# 		Player.gear[slot].speedBonus += self.speedIncrease

# class EnchantingScroll(Enchanting):
# 	def __init__(self,
# 			 itemName = "EnchantingScroll",
# 			 itemType = "Enchanting",
# 			 healAmount = 0,
# 			 damageIncrease = 1,
# 			 healthIncrease = 1,
# 			 defenseIncrease = 1,
# 			 speedIncrease = 1):

# 		kwargs = {"itemName" : itemName,
# 				  "itemType" : itemType,
# 				  "healAmount" : healAmount,
# 				  "damageIncrease" : damageIncrease,
# 				  "healthIncrease" : healthIncrease,
# 				  "defenseIncrease" : defenseIncrease,
# 				  "speedIncrease" : speedIncrease}

# 		Enchanting.__init__(self, **kwargs)

# class EnchantingPotion(Enchanting):
# 	def __init__(self,
# 			 itemName = "EnchantingPotition",
# 			 itemType = "Enchanting",
# 			 healAmount = 0,
# 			 damageIncrease = 1,
# 			 healthIncrease = 1,
# 			 defenseIncrease = 1,
# 			 speedIncrease = 1):

# 		kwargs = {"itemName" : itemName,
# 				  "itemType" : itemType,
# 				  "healAmount" : healAmount,
# 				  "damageIncrease" : damageIncrease,
# 				  "healthIncrease" : healthIncrease,
# 				  "defenseIncrease" : defenseIncrease,
# 				  "speedIncrease" : speedIncrease}
#
#		Enchanting.__init__(self, **kwargs)
