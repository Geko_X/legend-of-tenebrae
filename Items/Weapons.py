#####################################
#								    #
# Weapon classes, written by X.Hunt #
#								    #
#####################################

from Items import Gear
from .Classes.Log import log as Log

class Weapon(Gear):
	"""
	Weapon class. 
	This extends the gear class, and is extended by "specialised" weapon classes.

	Eg: Swords, maces, bows...
	"""
	def __init__(self, **kwargs):
		"""
		Class constructor
		"""
		Gear.__init__(self, **kwargs)


class Sword(Weapon):
	def __init__(self,
			 itemName = "Sword",
			 equipRegen = ["Right Hand"],
			 damageBonus = 1,
			 healthBonus = 0,
			 defenseBonus = 0,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Weapon.__init__(self, **kwargs)

# NOTE: Not actually used in game
class Bow(Weapon):
	def __init__(self,
			 itemName = "Bow",
			 equipRegen = ["Right Hand"],
			 damageBonus = 1,
			 healthBonus = 0,
			 defenseBonus = -1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Weapon.__init__(self, **kwargs)

class Mace(Weapon):
	def __init__(self,
			 itemName = "Mace",
			 equipRegen = ["Right Hand"],
			 damageBonus = 1,
			 healthBonus = 0,
			 defenseBonus = 0,
			 speedBonus = -1):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Weapon.__init__(self, **kwargs)