####################################
#								   #
# Armor classes, written by X.Hunt #
#								   #
####################################

from Items import Gear
from .Classes.Log import log as Log

class Armor(Gear):
	"""
	Armor class. 
	This extends the gear class, and is extended by "specialised" armor classes.

	Eg: Helms, shields, leggings...
	"""
	def __init__(self, **kwargs):
		"""
		Class constructor
		"""
		Gear.__init__(self, **kwargs)


class Helm(Armor):
	def __init__(self,
			 itemName = "Helm",
			 equipRegen = "Head",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)

class Chestplate(Armor):
	def __init__(self,
			 itemName = "Chestplate",
			 equipRegen = "Chest",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)

class Leggings(Armor):
	def __init__(self,
			 itemName = "Leggings",
			 equipRegen = "Legs",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)

class Boots(Armor):
	def __init__(self,
			 itemName = "Boots",
			 equipRegen = "Feet",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)

class Gloves(Armor):
	def __init__(self,
			 itemName = "Gloves",
			 equipRegen = "Gloves",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 1,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)

class Shield(Armor):
	def __init__(self,
			 itemName = "Shield",
			 equipRegen = "Left Hand",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 2,
			 speedBonus = 0):


		kwargs = {"itemName" : itemName,
				  "damageBonus" : damageBonus,
				  "equipRegen" : equipRegen,
				  "healthBonus" : healthBonus,
				  "defenseBonus" : defenseBonus,
				  "speedBonus" : speedBonus}

		Armor.__init__(self, **kwargs)