###################################
#								  #
# Item classes, written by X.Hunt #
#								  #
###################################

"""
Base item classes are defined here.
There are 2 types of items: Gear and Consumables

Gear are items that can be equipted, and will show when equipting items.
	Example: swords, shields...

Consumables are items that have one use and are then consumbed.
	Exmaple: Scroll of increased speed, health restoration potion

	HealthRestore items restore an health up to, but not over, the maximum value for health
	Increasing items increase an attribute's base value
	Enchanting items enchant the current equipted gear attributes by some amount

LIST OF INHERITANCE:

- Gear
 	- Weapon
 		- Sword
 		- Mace
 		- Bow
 	- Armor
 		- Head
 		- Chest
 		- Legs
 		- Boots
 		- Gloves

- Consumable
	- HealthRestore
		- Food
		- Potion
		- RestorationScroll
	Increasing
		- Potion
		- Scroll
	- Enchanting
		- EnchantingScroll
		- EnchantingPotion
"""

from .Classes.Log import log as Log

## Gear base class
class Gear(object):
	"""
	Base class for all gear.
	This extends on the item base class
	"""

	## Class constructor
	def __init__(self,
			 itemName = "Generic Gear",
			 equipRegen = "None",
			 damageBonus = 0,
			 healthBonus = 0,
			 defenseBonus = 0,
			 speedBonus = 0):
		"""
		Class constructor

		Parameters:
			- itemName:			The name of this item
			- equipRegen:		The regens that this item can be equipted it.
								This can be:
									- Head
									- Chest
									- Legs
									- Feet
									- Gloves
									- Left Hand
									- Right Hand
			- damageBonus:		The damage bonus that this item gives
			- healthBonus:		The health bonus that this item gives
			- defenseBonus:		The defense bonus that this item gives
			- speedBonus:		The speed bonus that this item gives
		"""

		self.name = itemName
		self.damageBonus = damageBonus
		self.equipRegen = equipRegen			# Typo here, I know. I found it after it was too late to change easily, however, so it stays
		self.healthBonus = healthBonus
		self.defenseBonus = defenseBonus
		self.speedBonus = speedBonus

	## Class constructor
	def __str__(self):
		"""
		Returns the item's name when the item object is used in a print statement
		"""
		return self.name

	## Class constructor
	def __repr__(self):
		"""
		Returns the item's name when the item object is represented as text
		"""
		return self.name

	## Uses the item by putting it in the equiped gear slot
	def Use(self, Player, index):
		"""
		Uses the item by putting it in the equiped gear slot

		Parameters:
			- Player:		The player to swap the gear on
			- index:		The index of this item
		"""

		slot = self.equipRegen

		oldItem = Player.gear[slot]
		Player.gear[slot] = self
		Player.updateGear()
		Player.removeFromInventory(index)
		Player.addToInventory(oldItem)


## Consumables base class
class Consumable(object):
	"""
	Base class for all consumbales.
	This extends on the item base class
	"""

	## Class constructor
	def __init__(self,
			 itemName = "Generic Consumable",
			 itemType = "Generic",
			 healAmount = 0,
			 damageIncrease = 0,
			 healthIncrease = 0,
			 defenseIncrease = 0,
			 speedIncrease = 0):

		"""
		Class constructor

		Parameters:
				- itemName:			The name of this item
				- itemType:			The type of item. This is used to determine how an item is displayed
									This can be:
										- HealthRestore
										- StatIncrease
										- Enchanting (not in final code)
				- damageIncrease:		The damage increase that this item gives
				- healthIncrease:		The health increase that this item gives
				- defenseIncrease:		The defense increase that this item gives
				- speedIncrease:		The speed increase that this item gives
		"""

		self.name = itemName
		self.type = itemType
		self.healAmount = healAmount
		self.damageIncrease = damageIncrease
		self.healthIncrease = healthIncrease
		self.defenseIncrease = defenseIncrease
		self.speedIncrease = speedIncrease

	## Class constructor
	def __str__(self):
		"""
		Returns the item's name when the item object is used in a print statement
		"""
		return self.name

	## Class constructor
	def __repr__(self):
		"""
		Returns the item's name when the item object is represented as text
		"""
		return self.name







