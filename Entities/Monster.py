####################################
#								   #
# Monster class, written by X.Hunt #
#								   #
####################################

"""
All monster classes (agressive entities) are defined here.

When creating a new monster, set values are used for each attribute, 
however, these can be overwritten when creating the monster.

Eg: 
	Standard zombie with set values. No params
		standardZombie = Zombie()

	Custom zombie. Has params
		superFastZombie = Zombie(speed = 100)
"""

from Entity import Entity
from .Classes.Vector import Vector2

####################################

## Base Monster class. Extends the base Entity class
class Monster(Entity):
	"""
	Base class for all agressive entities
	"""

	def __init__(self, **kwargs):
		"""
		Class constructor
		"""
		Entity.__init__(self, **kwargs)

	## Called each when the monster must attack
	def attack(self):
		"""
		Attack
		"""

		return self.damage

####################################

## Zombie monster
class Zombie(Monster):
	"""
	Zombie class
	"""

	def __init__(self,
				 entName = "Zombie",
				 char = "Z",
				 speed = 2,
				 moveRate = 4,
				 maxHealth = 20,
				 health = 15,
				 defense = 1,
				 damage = 3,
				 position = Vector2(0, 0),
				 drops = ["ZombieMeat", "RottenMeat"]):

		"""
		Class constructor. Passes a list of keyword arguments to create a new zombie
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position,
				  "drops" : drops}

		Monster.__init__(self, **kwargs)


####################################

## Skeleton monster
class Skeleton(Monster):
	"""
	Skeleton class
	"""

	def __init__(self,
				 entName = "Skeleton",
				 char = "S",
				 speed = 5,
				 moveRate = 4,
				 maxHealth = 12,
				 health = 12,
				 defense = 1,
				 damage = 4,
				 position = Vector2(0, 0),
				 drops = ["BoneSword", "BoneMace", "BoneShield", "BoneChest", "BoneLegs", "BoneHead"]):

		"""
		Class constructor. Passes a list of keyword arguments to create a new skeleton
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position,
				  "drops" : drops}

		Monster.__init__(self, **kwargs)


####################################

## Bat monster
class Bat(Monster):
	"""
	Bat class
	"""

	def __init__(self,
				 entName = "Bat",
				 char = "B",
				 speed = 5,
				 moveRate = 2,
				 maxHealth = 4,
				 health = 4,
				 defense = 0,
				 damage = 1,
				 position = Vector2(0, 0),
				 drops = ["BatMeat"]):

		"""
		Class constructor. Passes a list of keyword arguments to create a new bat
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position,
				  "drops" : drops}

		Monster.__init__(self, **kwargs)


####################################

## Rat monster
class Rat(Monster):
	"""
	Rat class
	"""

	def __init__(self,
				 entName = "Rat",
				 char = "R",
				 speed = 7,
				 moveRate = 2,
				 maxHealth = 2,
				 health = 2,
				 defense = 0,
				 damage = 1,
				 position = Vector2(0, 0),
				 drops = ["RatMeat", "FurBoots", "FurGloves"]):

		"""
		Class constructor. Passes a list of keyword arguments to create a new zombie
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position,
				  "drops" : drops}

		Monster.__init__(self, **kwargs)

####################################

## ItemDropper monster
class ItemDropper(Monster):
	"""
	ItemDropper class.

	This is a special monster that drops any item. It is always spawnable in any dungeon, with a spawn rate of 1%
	"""

	def __init__(self,
				 entName = "Item Dropper",
				 char = "I",
				 speed = 5,
				 moveRate = 5,
				 maxHealth = 20,
				 health = 20,
				 defense = 0,
				 damage = 3,
				 position = Vector2(0, 0),
				 drops = []):

		"""
		Class constructor. Passes a list of keyword arguments to create a new zombie
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position,
				  "drops" : drops}

		Monster.__init__(self, **kwargs)
