###################################
#								  #
# Entity class, written by X.Hunt #
#								  #
###################################

"""
This contains the basic class definition of entities.
It is extended by other entity classes.

Entites are generated when needed, similar to pokemon.
Each time the player steps, there is a chance that a battle will happen.
The entity will be generated, and the game state will change to being in battle.
"""

from .Classes.Vector import Vector2
from .Classes.Log import log as Log

import random
import math

## Base class for all entities
class Entity(object):

	"""
	Basic entity class. This is extended on by other classes
	"""

	## Class constructor
	def __init__(self,
				 entName = "Entity",
				 char = "E",
				 speed = 1,
				 moveRate = 4,
				 maxHealth = 10,
				 health = 10,
				 defense = 0,
				 damage = 1,
				 position = Vector2(0, 0),
				 drops = ["Meat"]):

		"""
		Class constructor

		Parameters:
			- entType:		The type of entity
			- entName:		The name for this entitiy
			- char:			The character used to represent this entity
			- speed:		The speed of this entity. Used to determine attacking order
			- maxHealth:	The maximum health this entity can have
			- health:		The current health of this entity. When this reaches 0, the entity dies
			- defense:		The defense points of this entity. Used to determine damage taken
			- damage:		The attacking points of this entity. Used to determine how much damage this entity will do
			- position:		The starting position of this entity
		"""

		## The name of this entity
		self.name = entName
		## Character used to represent this entity
		self.char = char
		## The position of this entity
		self.position = position
		## Is this entity alive?
		self.isAlive = True

		# Attributes

		## The speed of this entity. Used to determine attacking order
		self.speed = speed
		## The movement rate of this entity. 1 means that it will move every tick
		self.moveRate = moveRate
		## The maximum health this entity can have
		self.maxHealth = maxHealth
		## The current health of this entity. When this reaches 0, the entity dies
		self.health = health
		## The defense points of this entity. Used to determine damage taken
		self.defense = defense
		##The attacking points of this entity. Used to determine how much damage this entity will do
		self.damage = damage
		## What this monster drops when killed
		self.drops = drops

	## Called when the entitiy must attack
	def Attack(self):
		"""
		Called when the entity must attack.

		Return:
			False:			The attack missed
			Number:			The amount of damage dealt, if hit
		"""

		# Roll random for miss chance
		random.seed()

		if(random.random() < 0.05):
			return False

		return self.damage * random.uniform(0.95, 1.05)


	## Called each time the entity is hit
	def GetHit(self, damage):
		"""
		Called each time the entity is hit

		Parameters:
			- damage:		The ammount of damage to be dealt

		Return:
			The total damage dealt
		"""

		# Logic to determine the total damage delt
		totalDmg = (damage - self.defense)
		if(totalDmg < 0): totalDmg = 0

		self.health -= totalDmg

		return totalDmg

	## Heals the entity
	def Heal(self, amount):
		"""
		Called each time the entity needs to be healed

		Parameters:
			- amount:		The amount of health to restore
		"""

		self.health += amount

		# Check if over max health
		if(self.health > self.maxHealth):
			self.health = self.maxHealth

	## Called if the entity dies. Kills the entity
	def Die(self):
		"""
		Kills this entitiy
		"""
		self.isAlive = False

	## Called to move this entity
	def Move(self, direction):
		"""
		Move this entity in a given direction. All logic is done in the Main.py

		Parameters:
			- direction:	The direction and amount to move in
		"""

		self.position += direction

	## Returns true if the entity can move
	def canMove(self, tick):
		"""
		Returns true if the entity can move

		Return:
			True:			If can move
			False:			If cannot move
		"""

		return (tick % self.moveRate == 0)


