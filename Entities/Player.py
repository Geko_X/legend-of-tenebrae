###################################
#								  #
# Player class, written by X.Hunt #
#								  #
###################################

"""
The player class.

Extends the Entity class

"""

# Imports
from Entity import Entity as baseEntity
from .Classes.Vector import Vector2
from .Classes.Log import log as Log
from .Classes import Screen
import math

## Player class
class Player(baseEntity):
	"""
	Player class. Overrides most of the baseEntity methods
	"""

	def __init__(self,
				 entName = "Player",
				 char = u'\u263A',
				 speed = 4,
				 maxHealth = 100,
				 health = 100,
				 defense = 0,
				 damage = 3,
				 position = Vector2(0, 0)):

		"""
		Class constructor. Passes a list of keyword arguments to create a new player
		"""
		kwargs = {"entName" : entName,
				  "char" : char,
				  "speed" : speed,
				  "maxHealth" : maxHealth,
				  "health" : health,
				  "defense" : defense,
				  "damage" : damage,
				  "position" : position}

		baseEntity.__init__(self, **kwargs)

		# Player specific data

		## Color of the player
		self.color = "Cyan"

		## Player inventory, everything goes in here. List of items
		self.inventory = []

		## Equipped items. Dictionary of {Equipment slot : item}
		self.gear = {}

		self.baseSpeed = self.speed
		self.baseMaxHealth = self.maxHealth
		self.baseHealth = self.health
		self.baseDefense = self.defense
		self.baseDamage = self.damage
		self.currentDungeon = 0


	def Move(self, moveDir):
		"""
		Move the player in dir direction

		Paramters:
			- moveDir:			Vector2 added to the current position
		"""
		self.position += moveDir

	def addToInventory(self, item):
		"""
		Adds an item to the inventory
		"""

		# Only add an item to the inventory if there is room
		if(len(self.inventory) < 13):							# 12 is the size of the screen
		
			self.inventory.append(item)
			# Sorts by the name attribute
			self.inventory.sort(key = lambda x: x.name)

		else:
			Screen.writeLower(["Inventory is full", "Red"])

	def removeFromInventory(self, itemIndex):
		"""
		Removes an item from inventory
		"""

		if(len(self.inventory) < 1):
			return

		item = self.inventory[itemIndex]
		self.inventory.remove(item)

		# Sorts by the name attribute
		self.inventory.sort(key = lambda x: x.name)

		Log("Used item")

			
	def addToGear(self, item, slot):
		"""
		Adds an item to the selected equipment slot
		"""

		Log("Adding to gear:")
		Log(item)
		Log(slot)

		self.gear[slot] = item
		self.updateGear()

	def removeFromGear(self, slot):
		"""
		Removes an item from the equipment slot
		"""

		self.gear[slot] = ""
		self.updateGear()

	def updateGear(self):
		"""
		Updates the player attributes
		"""

		Log("Updating gear")

		damageBonus = 0
		healthBonus = 0
 		defenseBonus = 0
 		speedBonus = 0

		for slot, gear in self.gear.iteritems():
			damageBonus += gear.damageBonus
			healthBonus += gear.healthBonus
 			defenseBonus += gear.defenseBonus
 			speedBonus += gear.speedBonus

 		self.damage = self.baseDamage + damageBonus
 		self.maxHealth = self.baseMaxHealth + healthBonus
 		self.defense = self.baseDefense + defenseBonus
 		self.speed = self.baseSpeed + speedBonus

 		if(self.damage > 999): self.damage = 999
 		if(self.maxHealth > 999): self.maxHealth = 999
 		if(self.defense > 999): self.defense = 999
 		if(self.speed > 999): self.speed = 999

 		if(self.health > self.maxHealth):
 			self.health = self.maxHealth

 	def useItem(self, itemIndex):
 		"""
 		Uses an item
 		"""

 		item = self.inventory[itemIndex]
 		item.Use(self, itemIndex)

 		






		