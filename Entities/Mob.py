################################
#							   #
# Mob class, written by X.Hunt #
#							   #
################################

"""
All mob classes (passive entities) are defined here
"""

import Entity

####################################

## Base Mob class. Extends the base Entity class
class Mob(Entity.Entity):
	"""
	Base class for all passive entities
	"""

	pass

####################################

## Chicken Mob
class Chicken(Mob):
	"""
	Chicken class
	"""

	pass

####################################

