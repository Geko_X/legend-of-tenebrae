########################################
#									   #
# Dungeon Generator, written by X.Hunt #
#									   #
########################################

"""
Behold, the all powerful dungeon generation algorithms of algorithmic generation of dungeons!!
Without this script, there will be no dungeons!

By the power of multiprocessing, algorithms are faster!

BASIC OUTLINE OF GENERATING DUNGEONS:

	1. Create a new instance of a dungeon object
	2. Set the type, width and height of the dungeon
	3. Generate a blank dungeon
	4. Using the algorithm of type dunType, create the dungeon
	5. Assign the generated dungeon to the instantiated dungeon object

DUNGEON TYPES:
	0 - Blank
	1 - Drunkard walk
	2 - Recursive division
	3 - Cellular automation

NOTE: All algorithms are explained in the docstrings of the functions used to generate them

Each type gives a unique and distinctive feel.

	0 - Set every cell to a floor cell.
		Every grid cell is walkable.

	1 - Uses a "dunkard's walk".
		Creates natural and "staggering" caves.

	2 - Divides rooms into smaller rooms, smaller rooms into even smaller rooms.
		Gives a man made, square-like feel.

	3 - Literally grow a maze from a starting point.
		Gives rounded dungeons with nermous paths, many loops, and few dead ends

"""

########################################

# Imports

import random
import time
from multiprocessing import Process, Queue, Manager
from Vector import Vector2
from Log import log as Log
from Log import save2DArray as Save
from SaveLoad import SaveDungeon
import Name


##########

## Dungeons are objects with a type, width, height and depth
class Dungeon(object):
	"""
	Dungeon object. This is used to generate the dungeon
	"""

	## The type of dungeon to generate
	dunType = 0

	## The 3D array of dungeon data. Each cell contains [character, color pair name]
	dunData = []

	## The maximum height of the dungeon
	sizeY = 0
	## The maximum width of the dungeon
	sizeX = 0
	## The depth the dungeon will be
	depth = 0
	## Current floor
	currentFloor = 0
	## Previous floor
	previousFloor = -1
	## Name of this dungeon. Created from a file
	name = ""
	## The location of this dungeon
	location = Vector2(0, 0)

	## Base spawn rate for monsters
	BASE_SPAWN_RATE = 0.05
	## Current spawn rate
	currentSpawnRate = BASE_SPAWN_RATE

	## Class constructor
	def __init__(self, dunType, sizeY, sizeX, depth, ID, name = "Dungeon", location = Vector2(0, 0), monsterList = []):
		"""
		Class constructor

		Parameters:
			- dunType:			The type of dungon to generate
			- sizeY:			The maximum height of the dungeon
			- sizeX:			The maximum width of the dungeon
			- depth:			The depth the dungeon will be
		"""

		self.dunData = []

		self.type = dunType
		self.sizeY = sizeY
		self.sizeX = sizeX
		self.depth = depth
		self.ID = ID
		self.name = Name.getName()
		self.location = location

		self.monsterList = ["Zombie", "Skeleton", "Rat", "Bat"]

		Log("Dungeon object instantiated")
		self.CreateNew()

	########################################

	## Returns a floor tile to use
	def getFloorTile(self):
		"""
		Returns a floor tile to use

		Return:
			A floor tile
		"""

		tiles = [" "] * 1 + ["^"] * 5 + ["."] * 5
		return random.choice(tiles)

	## Returns a floor tile to use
	def getWallTile(self):
		"""
		Returns a wall tile to use

		Return:
			A wall tile
		"""
		tiles = [" "] * 50 + ["#"] * 1 + ["/"] * 1 + ["\\"] * 1 + ["'"] * 1
		return random.choice(tiles)

	########################################

	## Sets the current floor of the dungeon
	def setCurrentFloor(self, f):
		"""
		Sets the current floor of the dungeon

		Parameters:
			- f:				The floor to set to

		Return:
			True:				If successful
			False:				If failed
		"""

		if(f <= self.depth and f > 0):
			self.previousFloor = self.currentFloor
			self.currentFloor = f
			return True

		else:
			Log("Could not set current floor: Floor not in dungeon")
			return False

	## Returns a 2D array for the current floor
	def getCurrentFloor(self):
		"""
		Returns a 2D array for the current floor

		Return:
			Current floor:		If successful
			False:				If failed
		"""

		if(self.currentFloor == 0):
			Log("Could not get floor: Dungeon not generated")
			return False

		return self.dunData[self.currentFloor]

	########################################

	## Gets the monster spawn rate for this dungeon floor
	def getSpawnRate(self):
		"""
		Returns a percentage that a battle will occur when the player moves
		"""

		self.currentSpawnRate = self.BASE_SPAWN_RATE * (self.currentFloor)
		return self.currentSpawnRate

	########################################

	## Saves the dungeon to a .dun file
	def Save(self):
		SaveDungeon(self)

	########################################

	## Create a new blank dungeon
	def CreateNew(self):
		"""
		Creates the intial blank maze

		Returns:
			A new blank maze
		"""

		# Set the "header" index to a blank array
		self.dunData.append([])

		for d in xrange(self.depth):

			# Create a new floor
			floor = []

			for y in xrange(self.sizeY):
				floor.append([])

				for x in xrange(self.sizeX):
					floor[y].append([self.getWallTile(), "Wall"])

			# Add the newly generated floor to the dungeon data
			self.dunData.append(floor)
			#Save(floor, "New" + str(d))

		Log("Created new dungeon")

	## Generate a new dungeon of dungeon type type with seed seed
	def Generate(self, seed = 0):
		"""
		Generate the dungeon with a given seed.
		If seed is 0, a random seed is used

		Paramters:
			- seed:			The seed to use. Random if 0

		Returns:
			Newly generated dungeon as a 3D array of [character, color]
		"""

		start = time.time()

		# Set seed to random if 0
		if(seed == 0):
			seed = int(random.random() * 1000000)

		# Python doesnt have "proper" case selection, so this will have to do...

		# Blank type
		if(self.type == 0):
			self.dunData = self.genBlank()

		# Drunkard walk
		elif(self.type == 1):
			self.dunData = self.genDrunk(seed)

		# Recursive division
		elif(self.type == 2):
			self.dunData = self.genDiv(seed)

		# Cellular automation
		elif(self.type == 3):
			self.dunData = self.genCellAut(seed)

		# Invalid type
		else:
			Log("Error generating dungeon: Invalid type")
			self.dunData = 0

		end = time.time() - start
		
		Log("Finished creating dungeon. Took " + str(end) + "s")

		self.setCurrentFloor(1)

	################################
	# Dungen generation algorithms #
	################################

	## Generates a blank dungeon. Every cell is walkable except for the outer walls
	def genBlank(self):
		"""
		Sets every cell to a walkable floor cell. Outer cells remain as walls

		Returns:
			Blank dungeon as a 3D array of [character, color]
		"""

		## The new dungeon we are generating
		newDun = self.dunData

		for d in xrange(self.depth):

			## The current floor
			floor = []

			for y in xrange(self.sizeY):
				floor.append([])

				for x in xrange(self.sizeX):
					floor[y].append([self.getFloorTile(), "Floor"])

					if((y == 0) or (y == self.sizeY -1) or (x == 0) or (x == self.sizeX -1)):
						floor[y][x] = [self.getWallTile(), "Wall"]

			
			# Create start and end
			start = Vector2(2, 2)
			#end = Vector2(self.sizeY -2, self.sizeX -2)
			end = Vector2(4, 4)

			floor[int(start.y)][int(start.x)] = [u"\u2206", "Start"]
			floor[int(end.y)][int(end.x)] = [u"\u2207", "End"]

			newDun[0].append([start, end])

			# Add the newly generated floor to the dungeon data
			newDun[d + 1] = floor

			Save(floor, "Blank" + str(d))

		Log("Created blank dungeon")
		return newDun

	## Generates a new dungeon using the Drunkard's Walk algorithm
	def genDrunk(self, seed):
		"""
		Uses a "dunkard's walk" to generate natural and "staggering" caves.

		EXPLINATION:

			1. Pick a start and an end point

			2. Set the current cell to be the start or end point, depending on which pass we're in

			3. Set the current cell as a floor cell

			4. From the current point, choose a direction to move
				- This direction is biased towards the end point for first pass,
					and biased towards the start for the second pass
				- The direction will not be into a wall

			5. Set the current cell to be the current cell + the direction to move

			NOTE: Each pass is a seperate process, and are executed simultanously

		Parameters:
			- seed:			The seed to use

		Returns:
			New dungeon as a 3D array of [character, color]
		"""

		Log("Creating drunk dungeon")

		random.seed(seed)

		## One quater of the height
		quaterY = int(self.sizeY / 4)
		## One quater of the width
		quaterX	= int(self.sizeX / 4)

		## The new dungeon we are generating
		newDun = self.dunData

		# Sub process used to generate half of the dungeon
		def topHalf(floor, iterations, startPoint, endPoint, q):

			## The current cell
			currentCell = startPoint
			## The next cell
			nextCell = Vector2(0, 0)

			# For half of the iterations, go from start --> end
			for i in xrange(0, iterations / 2):

				# Set the current cell to a wall cell if it is on the edge, otherwise a floor cell
				if(int(currentCell.y) == 0) or (int(currentCell.y) == self.sizeY -1) or (int(currentCell.x) == 0) or (int(currentCell.x) == self.sizeX -1):
					floor[int(currentCell.y)][int(currentCell.x)] = [" ", "Wall"]

				else:
					floor[int(currentCell.y)][int(currentCell.x)] = [self.getFloorTile(), "Floor"]

				# Get the weights for each direction, checking that things are within the bounds of the dungeon
				if(currentCell.x <= 0):
					plusX = 5

				elif(currentCell.x >= self.sizeX -1):
					plusX = 0

				else:
					if(currentCell.x - endPoint.x < 0):
						plusX = 3
					else:
						plusX = 2

				
				if(currentCell.y <= 0):
					plusY = 5

				elif(currentCell.y >= self.sizeY -1):
					plusY = 0

				else:
					if(currentCell.y - endPoint.y < 0):
						plusY = 3
					else:
						plusY = 2

				minusX = 5 - plusX
				minusY = 5 - plusY

				## List of probabilities
				weightedDirection = [Vector2(1, 0)] * plusX + [Vector2(-1, 0)] * minusX + [Vector2(0, 1)] * plusY + [Vector2(0, -1)] * minusY

				## The direction to move in
				direction = random.choice(weightedDirection)

				currentCell += direction

			# This is now done, put the results in the queue
			q.put(["top", floor])

		# Subprocess used to generate half of the dungeon
		def lowHalf(floor, iterations, startPoint, endPoint, q):

			## The current cell
			currentCell = endPoint
			## The next cell
			nextCell = Vector2(0, 0)

			for i in xrange((iterations / 2) + 1, iterations):

				# Set the current cell to a wall cell if it is on the edge, otherwise a floor cell
				if(int(currentCell.y) == 0) or (int(currentCell.y) == self.sizeY -1) or (int(currentCell.x) == 0) or (int(currentCell.x) == self.sizeX -1):
					floor[int(currentCell.y)][int(currentCell.x)] = [" ", "Wall"]

				else:
					floor[int(currentCell.y)][int(currentCell.x)] = [self.getFloorTile(), "Floor"]

				if(currentCell.x <= 0):
					plusX = 5

				elif(currentCell.x >= self.sizeX -1):
					plusX = 0

				else:
					if(currentCell.x - endPoint.x > 0):
						plusX = 3
					else:
						plusX = 2

				
				if(currentCell.y <= 0):
					plusY = 5

				elif(currentCell.y >= self.sizeY -1):
					plusY = 0

				else:
					if(currentCell.y - endPoint.y > 0):
						plusY = 3
					else:
						plusY = 2


				minusX = 5 - plusX
				minusY = 5 - plusY

				## List of probabilities
				weightedDirection = [Vector2(1, 0)] * plusX + [Vector2(-1, 0)] * minusX + [Vector2(0, 1)] * plusY + [Vector2(0, -1)] * minusY

				## The direction to move in
				direction = random.choice(weightedDirection)

				currentCell += direction

			# This is now done, put the results in the queue
			q.put(["low", floor])

		def startEnd(floor, startPoint, endPoint):

			# Set the start and end points
			floor[int(startPoint.y)][int(startPoint.x)] = [u"\u2206", "Start"]
			floor[int(endPoint.y)][int(endPoint.x)] = [u"\u2207", "End"]
			newDun[0].append([startPoint, endPoint])

		# Store the time so we can determine how long this takes
		now = time.time()

		for d in xrange(1, self.depth + 1):

			## The start point. This will always be in the upper left quadrant
			startPoint = Vector2(1 + random.randint(1, quaterX), 1 + random.randint(1, quaterX))
			## The end point. This will always be in the lower right quadrant
			endPoint = Vector2(random.randint(quaterX, self.sizeX -1) -1, random.randint(quaterY * 3, self.sizeY -1) -1)

			## Flag
			iterations = int((self.sizeX + self.sizeY) * 250)

			## Current floor
			floor = newDun[d]

			# The queue that the results are saved to
			q = Queue()

			# Create the processes
			top = Process(target = eval("topHalf"), args = (floor, iterations, startPoint, endPoint, q))
			low = Process(target = eval("lowHalf"), args = (floor, iterations, startPoint, endPoint, q))

			# Start the processes
			top.start()
			low.start()

			# Retreive the results from each process
			part1 = q.get()
			part2 = q.get()

			# Stop the proccesses
			top.join()
			low.join()

			# As the processes may finish in unexpected orders, we need to check each result to see where it goes. 
			if(part1[0] == "top"):
				floor[:self.sizeY -1] = part1[1]
				floor[self.sizeY:] = part2[1]

			else:
				floor[:self.sizeY -1] = part2[1]
				floor[self.sizeY:] = part1[1]

			# Add the start and end for this floor
			startEnd(floor, startPoint, endPoint)

			# Update the data with this floor
			newDun[d] = floor

			Log("Floor " + str(d) + " done")

		taken = time.time() - now

		Log("Drunkard's Walk generation completed")

		return newDun


				












