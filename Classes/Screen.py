################################
#							   #
# Screen.py, written by X.Hunt #
#							   #
################################

# Handles functions for drawing to the screen

"""
Handles all screen releated functions, including:
	- Creating windows (refered to as screens)
	- Drawing to screens

This also handles all color methods, 
as well as holding a dictinary of {name, color pair ID} and a list of [color pair names].

A color pair is a pair of colors used by curses to add color to the screen.
A pair is defined as an:
	- id (starting at 1, as 0 is default white on black). 
		This id is used internally and is not exposed in code
	- foreground (one of 8 possible colors)
	- background (one of 8 possible colors)

Color pairs are also used to "tag" a tile.
For example, a water tile will have the "Water" color pair, and a floor tile will have the "Floor" color pair

Color pairs are defined, and then added to a dictionary.
The dictionary is defined as:
	Key: String
	Value: Integer

The value is the same as the id for that particular color pair.

Each pairID is keyed by it's name, 
	eg: "Water" is a color pair with blue foreground and cyan background.
		Colors["Water"] => 5

Color codes:
	0 - Black
	1 - Red
	2 - Green
	3 - Yellow
	4 - Blue
	5 - Magenta
	6 - Cyan
	7 - White
"""

import curses as c
import sys
from collections import deque			# Used for the message queue
from Log import log as Log


#############
# Variables #
#############

## Array of all screens
allScreens = []

## The whole screen available
mainScreen = None
## The screen the game is displayed in
gameScreen = None
## The screen off to the side
sideScreen = None
## The lower screen
lowerScreen = None
## List of all messages to show in the lower screen
lowerScreenMsg = None
## The title screen
titleScreen = None

## The title of the screen
gameTitle = "Title"


## Flag for if the screens have been initialised
hasInitalised = False

## Global color dictionary.
## This is used to loook up a calor pair from a name
colors = {}

## Global color name list.
## This is a list containing all the names of color pairs
colorList = []

## List of colors availble to use
usableColors = [c.COLOR_BLACK, 
				c.COLOR_RED, 
				c.COLOR_GREEN, 
				c.COLOR_YELLOW, 
				c.COLOR_BLUE, 
				c.COLOR_MAGENTA, 
				c.COLOR_CYAN, 
				c.COLOR_WHITE]

###################
# Color Functions #
###################

## Get a color pair by name
def getColorPair(name):
	"""
	Returns the color pair ID with the given name

	Parameters:
		- name:			The name of the color pair

	Return:
		- Integer representing the given color pair name
	"""
	return colors[name]

## Add a color pair to the list of usable color pairs
def addColorPair(name, foreground, background):
	"""
	Adds a color pair to the dictionary of colors

	Parameters:
		- name:			The name of the color pair
		- foreground:	The color code for the foreground
		- background:	The color code for the background
	"""

	## The ID of the color pair
	pairID = len(colors) + 1

	# Create the color pair
	c.init_pair(pairID, usableColors[foreground], usableColors[background])

	# Add the color pair to the dictionary and list
	colors[name] = pairID
	colorList.append(name)

## Get the list of all color pairs			(Not used?)
def getColorList():
	"""
	Returns a list of all color pair names

	Return: 
		- List containing all color pair names
	"""
	return colorList

## Creates the initial list of color pairs
def createInitialColors():
	"""
	Creates the initial color pairs
	"""

	# Standard colors
	addColorPair("Black", 0, 0)			# Black - no color
	addColorPair("Red", 1, 0)			# Red on black
	addColorPair("Green", 2, 0)			# Green on black
	addColorPair("Yellow", 3, 0)		# Yellow on black
	addColorPair("Blue", 4, 0)			# Blue on black
	addColorPair("Magenta", 5, 0)		# Magenta on black
	addColorPair("Cyan", 6, 0)			# Cyan on black
	addColorPair("White", 7, 0)			# White on black
	
	# Colors used for tagging, as well as cusotm colors
	addColorPair("Water", 6, 0)			# Cyan on black
	addColorPair("Floor", 3, 0)			# Yellow on black
	addColorPair("Wall", 0, 7)			# Black on white
	addColorPair("Start", 2, 0)			# Green on black
	addColorPair("End", 1, 0)			# Red on black
	addColorPair("Dirt", 3, 0)			# Yellow on black
	addColorPair("Grass", 2, 0)			# Green on black
	addColorPair("Dungeon", 1, 0)		# Red on black
	

####################
# Screen functions #
####################

## Outputs info to the debug box, and then logs to the log file
def debugBox(message):
	"""
	Displays a debug message, and appends to log file.

	This function is "disabled" in the final game code (still implemnted, but does not update the debug box)

	Parameters:
		- message:		The message to be displayed
	"""
	# Clear the debug space
	if(hasInitalised):

		blank = "=" * (mainScreen.getmaxyx()[1] -1)

		mainScreen.addstr(mainScreen.getmaxyx()[0] -1, 0, blank, c.color_pair(colors["Black"]))
		#mainScreen.addstr(mainScreen.getmaxyx()[0] -1, 0, message, c.color_pair(colors["Yellow"]))

		mainScreen.refresh()

	else:
		print message

	# Log to file
	Log(message)

## Refreshes the game screen
def refreshGame(offSetY = 0, offSetX = 0):
	"""
	Scrolls the game pad to show the correct part of the game

	Paramters:
		- startPoint:	List of [integerY, integerX] used to set the start point
		- endPoint:	List of [integerY, integerX] used to set the end point
	"""


	#debugBox("Refreshing game")

	sizeY = int(mainScreen.getmaxyx()[0] * 0.25) * 3 -1
	sizeX = int(mainScreen.getmaxyx()[1] * 0.25) * 3 -1
	
	try:
		endPoint = [gameScreen.getmaxyx()[0], gameScreen.getmaxyx()[1]]
		gameScreen.refresh(0,0, 1,1, (sizeY * 3), (sizeX * 3) -1)



	except Exception, e:
		debugBox("Error refreshing game: " + str(e))

## Refreshes all screens, except the game screen
def refresh():
	"""
	Refreshs all screens, except the game screen
	"""

	#debugBox("Refreshing")

	if(hasInitalised):
		for screen in allScreens:

			# If the screen is a conventional screen (a window), the refresh() function needs no paramters
			try:
				screen.noutrefresh()

			# If it is a pad, 
			except:
				refreshGame()

		c.doupdate()

	else:
		print "Error refreshing screens - screens not initialised"

## Creates all screens
def createScreens():
	"""
	Creates all the game screens.
	Sets the hasInitalised flag to True when complete
	"""

	# Globals so we can modify things
	global hasInitalised

	global mainScreen
	global gameScreen
	global sideScreen
	global lowerScreen

	global lowerScreenMsg

	try:

		# Create the main screen
		mainScreen = c.initscr()
		mainScreen.nodelay(True)
		mainScreen.keypad(True)

		# Get one quater of the availble screen size.
		# This is used to draw other screens
		sizeY = int(mainScreen.getmaxyx()[0] * 0.25)
		sizeX = int(mainScreen.getmaxyx()[1] * 0.25)

		# Create the other screens
		gameScreen = c.newpad((sizeY * 3) -1, (sizeX * 3) -2)
		sideScreen = c.newwin((sizeY * 3) -1, sizeX, 1, sizeX * 3)
		lowerScreen = c.newwin(sizeY, sizeX * 4, sizeY * 3, 0)

		# Add all screens to the array of all screens
		allScreens.append(gameScreen)
		allScreens.append(mainScreen)
		allScreens.append(sideScreen)
		allScreens.append(lowerScreen)
		

		# Draw a frame around all screens, except the main screen
		for i in xrange(2, 4):
			allScreens[i].border("|", "|", "-", "-")

		# If we've got this far, everything has worked correctly and is properly initalised
		hasInitalised = True
		debugBox("Screens initialised")

		# Start the log steam
		lowerScreenMsg = deque([["Log stream dummy", "Black"]] * lowerScreen.getmaxyx()[0])

		# Refresh the screens
		refresh()
		refreshGame()

	except Exception, e:
		print "Error initialising screens: " + str(e)

## Title screen
def showTitleScreen(savedGame = False):
	"""
	Shows the title screen
	"""

	global titleScreen

	# Create the main screen
	titleScreen = c.initscr()
	titleScreen.nodelay(True)
	titleScreen.keypad(True)

	titleText = [["Legend of Tenebrae", "Green"],
				[" ", "Black"],
				[" ", "Black"],
				[" ", "Black"],
				[" ", "Black"],
				[" ", "Black"],
				[" ", "Black"],
				[" ", "Black"],
				["Z: New Game", "Yellow"], 
				["C: Continue Game", "Yellow"]]

	# Remove the continue option if there is no saved game
	if(not savedGame):
		titleText = titleText[:-1]

	# The main title is underlined, so it needs to be written separately
	text = titleText[0] 
	titleScreen.addstr(int(titleScreen.getmaxyx()[0] * 0.25), (titleScreen.getmaxyx()[1] - len(text[0])) /2, text[0], c.color_pair(colors[text[1]]) | c.A_UNDERLINE | c.A_BOLD)

	for i in xrange(1, len(titleText)):
		text = titleText[i]
		titleScreen.addstr(int(titleScreen.getmaxyx()[0] * 0.25) + i, (titleScreen.getmaxyx()[1] - len(text[0])) /2, text[0], c.color_pair(colors[text[1]]))

	# The "by line" down the bottom
	text = ["A game by Xavier Hunt for year 12 Software", "Blue"]
	titleScreen.addstr(titleScreen.getmaxyx()[0] -1, (titleScreen.getmaxyx()[1] - len(text[0])) /2, text[0], c.color_pair(colors[text[1]]))

## Kills the title screen
def endTitleScreen():
	"""
	Ends the title screen, ready for the main screens to take over
	"""
	
	global titleScreen
	del titleScreen


## Kills alll the screens, and sets the terminal back to what it should be
def endScreens():
	"""
	Puts the terminal back to a useable state. Called when the program ends
	"""

	if(hasInitalised):

		for screen in allScreens:
			screen.clear()
		
		refresh()
		refreshGame()

		mainScreen.nodelay(False)

		c.curs_set(True)
		c.nocbreak()
		c.echo()	
		c.endwin()

## Sets the game title
def setTitle(title, changeWindowTitle = True):
	"""
	Sets the title of the game

	Parameters:
		- title:		The new title
	"""

	global gameTitle

	if(not hasInitalised):
		createScreens()
		setTitle(title, changeWindowTitle)

	else:
		try:
			gameTitle = title

			mainScreen.addstr(0, 0, " " * mainScreen.getmaxyx()[1])
			mainScreen.addstr(0, (mainScreen.getmaxyx()[1] - len(title)) / 2, title, c.color_pair(colors["Green"]) | c.A_UNDERLINE)

			mainScreen.refresh()

			if(changeWindowTitle):
				s = "\x1b]2;%s\x07" %(title)
				sys.stdout.write(s)

		except Exception, e:
			er = "Error setting title: " + str(e)
			debugBox(er)

## Displays text to the game screen
def writeGame(text):
	"""
	Writes text to the game screen

	Parameters:
		- text:			The text to write
	"""
	gameScreen.clear()
	gameScreen.addstr(gameScreen.getmaxyx[0] /2, (gameScreen.getmaxyx[1] + len(text) /2, text))

## Draws the game, and then calls to refresh the screens
def drawGame(grid):
	"""
	Draws the game screen

	Parameters:
		- grid:			2D list[][] containing a list of [the world to draw, and its color]
	"""
	try:
		for y in xrange(len(grid)):
			for x in xrange(len(grid[y])):

				data = grid[y][x]
				pair = getColorPair(data[1])
				text = data[0].encode("utf-8")

				if(data[1] == "Dungeon" or data[1] == "Water" or data[1] == "Start" or data[1] == "End"):
					gameScreen.addstr(1 + y, 1 + (x * 2), text, c.color_pair(pair) | c.A_BOLD)

				else:
					gameScreen.addstr(1 + y, 1 + (x * 2), text, c.color_pair(pair))

				gameScreen.addch(1 + y, (x * 2), " ", c.color_pair(pair))
				
				refreshGame()

		#debugBox("Drew game")

	except Exception, e:
		debugBox("Error drawing game: " + str(e))

## Draws the side screen, and then calls to refresh the screens
def drawSide(data):
	"""
	Draws the side screen

	Parameters:
		- data:			List[] containing a list of [the text to draw, and its color]
	"""
	try:

		for y in xrange(len(data)):

			textData = data[y]
			pair = getColorPair(textData[1])
			text = textData[0].encode("utf-8")

			sideScreen.addstr(1 + y, 1, text, c.color_pair(pair))
			
			refresh()

		sideScreen.border("|", "|", "-", "-")
		#debugBox("Drew side")

	except Exception, e:
		debugBox("Error drawing side: " + str(e))
	

## Writes to the lower screen
def writeLower(info):
	"""
	Writes a message to the lower screen, and moves previous messages up

	Parameters:
		- info:		The message and color to write to the screen
	"""

	global lowerScreenMsg

	lowerScreenMsg.append(info)

	Log("Game info: " + info[0])

	# Check to see if the message queue is longer than what we can show
	# If it is, pop the queue to the left (removes the oldest message)
	if(len(lowerScreenMsg) > lowerScreen.getmaxyx()[0] - 2):
		lowerScreenMsg.popleft()

	try:
		for i  in xrange(lowerScreen.getmaxyx()[0] - 2, 0, -1):

			# Get the message and the color
			msg = lowerScreenMsg[i + 1][0]
			col = lowerScreenMsg[i + 1][1]

			# Clear the line and then write the message
			lowerScreen.addstr(i, 1, " " * (lowerScreen.getmaxyx()[1] -2))
			lowerScreen.addstr(i, 1, msg, c.color_pair(getColorPair(col)))

	except Exception, e:
		debugBox(("Error writing lower: " + str(e)))

## Draws the lower screen, and then calls to refresh the screens
def drawLower(grid):
	"""
	Draws the lower screen

	Parameters:
		- grid:			2D list[][] containing a list of [the world to draw, and its color]
	"""
	try:
		for y in xrange(len(grid)):
			for x in xrange(len(grid[y])):

				data = grid[y][x]
				pair = getColorPair(data[1])
				text = data[0].encode("utf-8")

				lowerScreen.addstr(1 + y, 1 + x, text, c.color_pair(pair))
				
				refresh()

		#debugBox("Drew lower")

	except Exception, e:
		debugBox(("Error drawing lower: " + str(e)))

###############################

## Get input from the player
def getPlayerInput():
	"""
	Gets input from the player. Called once each loop

	Returns:
		A keycode for the key that was pressed
	"""

	if(mainScreen != None):
		key = mainScreen.getch()
	else:
		key = titleScreen.getch()
	return key

if __name__ == '__main__':
	debugBox("debug")
	endScreens()

