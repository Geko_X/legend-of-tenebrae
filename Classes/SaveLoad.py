##################################
#								 #
# SaveLoad.py, written by X.Hunt #
#								 #
##################################

"""
Handles all saving and loading functions.

===========================================================================================

Dungeons are saved to .dun files.

A .dun file looks like:

Line1: ID, name, size of the dungeon, and overworld location of the dungeon
Line2: Spawn rate and then a list of monsters spawnable in this dungeon
Line3: An index of the where each dungeon floor starts
Line4: Blank
Line5: Floor1 start, end
Next lines are a map of the dungeon
Blank line
Floor2 start, end
Next lines are a map of the dungeon

The folowing characters are used to save tile data
w 	- Wall
' ' - Floor
S 	- Start
E 	- End

=============

Example extract of a .dun file

0,Dungeon1,10,10,42,27
0.05,Zombie,Skeleton
5,17

3,4,7,5
w,w,w,w,w,w,w,w,w,w
w, , ,w,w,w, , ,w,w
w, , ,w, , , ,w, ,w
w, , , , ,w, , , ,w
w,w, ,S, ,w, , , ,w
w, , , , ,w, ,w, ,w
w,w,w, ,w, ,w,w,w,w
w,w,w, , ,E, , , ,w
w,w,w,w, , , , , ,w
w,w,w,w,w,w,w,w,w,w

1,5,9,7
w,w,w,w,w,w,w,w,w,w

===========================================================================================

The player is saved to a .plr file.

A .plr file consists of:

Line1: CurrentHealth
Line2: BaseHealth
Line3: BaseDamage
Line4: BaseDefense
Line5: BaseSpeed
Line6: Invetory as a list of item LookUpNames (from items.txt)
Line7: Gear as an ordered list of LookUpNames, sorted by slot

===========================================================================================

The world is saved to a .wrld file.

A .wrld file is similar to a .dun file, except the header data contains dungeon locations and player spawn

Line1: Size of dungeon
Line2: List of dungeon locations
Line3: Player spawn location
Line4: Blank

Next lines are a map of the world

"""

#######################

import sys, os
from .png import png
import time
from multiprocessing import Process, Queue

import DungeonGen
import WorldGen
from Vector import Vector2
from Log import log as Log
from GetItem import getLookUpName as GetLookUpName
from GetItem import getItem as GetItem
from .Entities.Player import Player as gamePlayer

#######################

# Dictionary to convert game tile tags to save tile data
tagData = {"Floor" : " ",
			"Wall" : "w",
			"Start" : "S",
			"End" : "E",
			"Dungeon" : "D",
			"Water" : "~",
			"Dirt" : ".",
			"Grass" : "^"
			}

# Reversed tag data
reverseTagData = dict((value, key) for key, value in tagData.iteritems())

###################################################################################################################

## Saves a dungeon to a .dun file
def SaveDungeon(dun):
	"""
	Saves a dungeon to a .dun file.
	The save happens in a different process so it does not impact on game speed

	Paramters:
		- dun:				The dungeon object to save
	"""

	if(type(dun) != DungeonGen.Dungeon):
		return False

	Log("Starting save")
	start = time.time()

	ID = dun.ID
	name = dun.name

	sizeY = dun.sizeY
	sizeX = dun.sizeX
	depth = dun.depth
	loc = dun.location

	data = dun.dunData

	mons = dun.monsterList
	baseSpawnRate = dun.BASE_SPAWN_RATE

	# Create the data to write

	filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/%s.dun" %(name)

	with open(filePath, "w") as saveFile:

		# Write name and locations of the dungeon
		line = str(ID) + "," + name + "," + str(sizeY) + "," + str(sizeX) + "," + str(depth) + "," + str(int(loc.x)) + "," + str(int(loc.y))
		saveFile.write(line)
		saveFile.write("\n")

		# Write spawn rate and monsters
		line = str(baseSpawnRate)
		for m in mons:
			line += "," + str(m)
		saveFile.write(line)
		saveFile.write("\n")

		# Write where each floor starts
		line = "5"
		for d in xrange(1, depth):
			line +=  "," + str((d * sizeY) + 5 + (d*2))

		saveFile.write(line)
		saveFile.write("\n")

		# Iterate though each floor
		for d in xrange(depth):

			# Write the start and end locations
			saveFile.write("\n")
			line = str(data[0][d][0].x) + "," + str(data[0][d][0].y) + "," + str(data[0][d][1].x) + "," + str(data[0][d][1].y) + "\n"
			saveFile.write(line)

			# Write the floor data
			for y in xrange(sizeY):
				line = ""

				for x in xrange(sizeX):
					tag = str(data[d+1][y][x][1])

					line += "," + tagData[tag]

				line = line[1:]
				line += "\n"
				saveFile.write(line)

	end = time.time() - start
	Log("Finished save. Took " + str(end) + "s")

## Loads a dungeon from a .dun file
def LoadDungeon(filename):
	"""
	Loads a dungeon from a .dun file.
	The save happens in a different process so it does not impact on game speed.

	This generates a new blank dungeon and then changes the data to match the save data

	Paramters:
		- fileName:				The dungeon savename to laod
	"""

	def __load(filename):

		# Start the timer
		start = time.time()

		# The save data
		saveData = None

		filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/%s.dun" %(filename)
		with open(filePath, "r") as saveFile:
			saveData = saveFile.read()

		saveData = saveData.split("\n")

		# Get the first line
		lineData = saveData[0].split(",")

		# Get data from the first line and create a blank dungeon with it
		ID = int(lineData[0])
		name = lineData[1]
		sizeY = int(lineData[2])
		sizeX = int(lineData[3])
		depth = int(lineData[4])
		location = Vector2(int(lineData[5]), int(lineData[6]))

		dun = DungeonGen.Dungeon(0, sizeY, sizeX, depth, ID, "BlankDungeon")
		dun.Generate()
		dun.name = name

		# Add the monster spawn data (line 2) to the dungeon
		dungeonInfo = saveData[1].split(",")
		dun.BASE_SPAWN_RATE = float(dungeonInfo[0])

		# List of all spawnable monsters for this dungeon
		dun.monsterList = []

		for m in xrange(1, len(dungeonInfo)):
			dun.monsterList.append(dungeonInfo[m])

		# Get the positions of all the floor starts so we can jump to them in the file
		floorStarts = saveData[2].split(",")

		# The bulk of the load. Iterate through the remaining data and create the dungeon floors from it
		for d in xrange(depth):

			# Get the line this floor starts at, and copy the approriate segment to a temporary list
			thisFloorStart = int(floorStarts[d]) -1
			thisFloor = saveData[thisFloorStart : thisFloorStart + sizeY]

			thisFloorLocations = thisFloor[0].split(",")

			# Set the start and end location of this floor
			dun.dunData[0][d][0] = Vector2(int(float(thisFloorLocations[0])), int(float(thisFloorLocations[1])))
			dun.dunData[0][d][1] = Vector2(int(float(thisFloorLocations[2])), int(float(thisFloorLocations[3])))

			# Remove the first index of the floor
			thisFloor = thisFloor[1:]

			y = 0

			# Get the saved character and create a dungeon from it
			for line in thisFloor:
				line = line.split(",")

				x = 0

				# For each character in line
				for c in line:

					# Get the tag from the character
					tag = reverseTagData[c]
					tile = "!"

					# Get the tile from the tag
					if(tag == "Wall"):
						tile = dun.getWallTile()
					if(tag == "Floor"):
						tile = dun.getFloorTile()
					if(tag == "Start"):
						tile = u"\u2206"
					if(tag == "End"):
						tile = u"\u2207"

					# Set the current dungeon tile and tag
					dun.dunData[d+1][y][x] = [tile, tag]

					x += 1
				y += 1	

		end = time.time() - start
		Log("Finished load. Took " + str(end) + "s")

		return dun

	#try:
	dun = __load(filename)
	#except Exception, e:
	#	msg = "Error loading dungeon: " + str(e)
	#	Log(msg)
	#	Log("FileName: " + str(filename))
		
	return dun

###################################################################################################################

## Saves a player to a .plr file
def SavePlayer(player):
	"""
	Saves a player to a .plr file

	Line1: CurrentHealth
	Line2: BaseHealth
	Line3: BaseDamage
	Line4: BaseDefense
	Line5: BaseSpeed
	Line6: Invetory as a list of item LookUpNames (from items.txt)
	Line7: Gear as an ordered list of LookUpNames, sorted by slot

	"""

	Log("Saving player")

	if(type(player) != gamePlayer):
		return

	# Start the timer
	start = time.time()

	filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/player.plr"
	with open(filePath, "w") as saveFile:

		# Write the base attributes
		saveFile.write(str(player.health))
		saveFile.write("\n")
		saveFile.write(str(player.baseMaxHealth))
		saveFile.write("\n")
		saveFile.write(str(player.baseDamage))
		saveFile.write("\n")
		saveFile.write(str(player.baseDefense))
		saveFile.write("\n")
		saveFile.write(str(player.baseSpeed))
		saveFile.write("\n")

		# Write the inventory
		data = ""
		for item in player.inventory:

			itemLookUp = GetLookUpName(item.name)
			data += itemLookUp + ","

		# Get rid of the trailing comma
		data = data[:-1]
		saveFile.write(data)
		saveFile.write("\n")

		# Write the gear
		data = ""

		data += GetLookUpName(player.gear["Head"].name) + ","
		data += GetLookUpName(player.gear["Chest"].name) + ","
		data += GetLookUpName(player.gear["Legs"].name) + ","
		data += GetLookUpName(player.gear["Feet"].name) + ","
		data += GetLookUpName(player.gear["Gloves"].name) + ","
		data += GetLookUpName(player.gear["Left Hand"].name) + ","
		data += GetLookUpName(player.gear["Right Hand"].name)

		saveFile.write(data)
		saveFile.write("\n")

		# Write the current dungeon
		saveFile.write(str(player.currentDungeon))

		end = time.time() - start
		Log("Finished save. Took " + str(end) + "s")

## Loads the player from file
def LoadPlayer():
	"""
	Load a player from file
	"""

	Log("Loading player")

	player = None

	# Start the timer
	start = time.time()

	filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/player.plr"
	with open(filePath, "r") as saveFile:

		saveData = saveFile.read().split("\n")

		# Load attributes
		currentHealth = int(saveData[0])
		baseMaxHealth = int(saveData[1])
		baseDamage = int(saveData[2])
		baseDefense = int(saveData[3])
		baseSpeed = int(saveData[4])

		player = gamePlayer("Player", health = baseMaxHealth, damage = baseDamage, defense = baseDefense, speed = baseSpeed)
		player.health = currentHealth
		player.currentDungeon = int(saveData[7])

		# Load inventory
		for itemLookup in saveData[5].split(","):
			item = GetItem(itemLookup)
			player.addToInventory(item)

		# Load gear
		for itemLookup in saveData[6].split(","):
			item = GetItem(itemLookup)
			player.addToGear(item, item.equipRegen)

	return player

###################################################################################################################

## Save the world to a .wrld file
def SaveWorld(world):
	"""
	Saves the world to a .wrld file

	A .wrld file is similar to a .dun file, except the header data contains dungeon locations and player spawn

	Line1: Name and size of dungeon
	Line2: List of dungeon locations
	Line3: Player spawn location
	Line4: Blank
	Rest of the data is the world data, as a list of [tileData, height]
	"""

	if(type(world) != WorldGen.World):
		return False

	Log("Starting world save")
	start = time.time()

	name = world.name

	sizeY = world.sizeY
	sizeX = world.sizeX

	data = world.worldData

	dunLocations = world.dungeonLocations
	playerSpawn = world.playerSpawn

	# Create the data to write

	filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/world.wrld"
	with open(filePath, "w") as saveFile:

		# Name and sizes
		line = name + ",%i,%i\n" %(world.sizeX, world.sizeY)
		saveFile.write(line)

		line = ""

		# Dungeon locations
		for loc in dunLocations:
			line += str(loc.x) + ":" + str(loc.y) + ","

		line = line[:-1] + "\n"
		saveFile.write(line)

		# Player spawn
		line = str(playerSpawn.x) + ":" + str(playerSpawn.y)
		saveFile.write(line)

		saveFile.write("\n")
		saveFile.write("\n")

		# World data
		for y in xrange(world.sizeY):
			line = ""
			for x in xrange(world.sizeX):

				tag = world.worldData[y][x][1]
				height = world.worldData[y][x][2]

				char = tagData[tag]

				line += "%s:%i," %(char, height)
			line = line[:-1] + "\n"
			saveFile.write(line)

## Loads a world from a .wrld file
def LoadWorld():
	"""
	Loads a world from a .wrld file.

	A new world is generated and data is copied into it
	"""

	# Read the data
	filePath = os.path.dirname(sys.argv[0]) + "/Data/Saves/world.wrld"
	with open(filePath, "r") as saveFile:
		saveData = saveFile.read().split("\n")

		# Load name and size
		lineData = saveData[0].split(",")
		name = lineData[0]
		sizeX = int(lineData[1])
		sizeY = int(lineData[2])

		# Load dungeon locations
		lineData = saveData[1].split(",")
		dunLocations = []

		for data in lineData:
			rawXY = data.split(":")
			x = float(rawXY[0])
			y = float(rawXY[1])

			loc = Vector2(x, y)
			dunLocations.append(loc)

		# Load Player spawn location
		lineData = saveData[2]
		rawXY = lineData.split(":")
		x = float(rawXY[0])
		y = float(rawXY[1])

		playerSpawn = Vector2(x, y)

		# Create a new world
		world = WorldGen.World(sizeX, sizeY)
		world.Generate()

		# Set loaded data to the new world
		world.name = name
		world.dungeonLocations = dunLocations
		world.playerSpawn = playerSpawn

		# Iterate through the remaining data and assign it to the world
		saveData = saveData[4:]
		
		y = 0

		for line in saveData[:-1]:
			line = line.split(",")

			x = 0

			# For tileData in line
			for tileData in line:

				# Get the tag from the character
				tag = reverseTagData[tileData[0]]
				height = tileData[1]
				tile = "!"

				# Get the tile from the tag
				if(tag == "Dirt" or tag == "Grass"):
					tile = world.getGroundTile()
				if(tag == "Water"):
					tile = "~"
				if(tag == "Dungeon"):
					tile = u"\u2206"

				# Set the current dungeon tile and tag
				world.worldData[y][x] = [tile, tag, height]

				x += 1
			y += 1

	return world


## Save the world to a .png file
def SaveWorldPng(world):

	Log("Saving world to PNG")

	return

	pngOut = []
	tempPngOut = []
	
	for y in xrange(world.sizeY -1):

		line = []

		for x in xrange(world.sizeX -1):

			line.append(world.worldData[y][x][2])
			line.append(world.worldData[y][x][2])
			line.append(world.worldData[y][x][2])

			tempPngOut.append(line)

			Log(tempPngOut)

			pngOut.append(tuple(tempPngOut))

	Log(pngOut)

	w = png.Writer(world.sizeX, world.sizeY)

	filePath = os.path.dirname(sys.argv[0]) + "/Data/Maps/World Map.png"

	f = open(filePath, "w+")
	w.write(f, pngOut)
	w.close













