######################################
#									 #
# Wolrd Generator, written by X.Hunt #
#									 #
######################################

"""
Behold, the all powerful world generation algorithm of algorithmic generation of the world!!
Without this script, there will be no world!

By the power of multiprocessing, algorithms are faster!

BASIC OUTLINE OF GENERATING A WORLD:

	1. Create a new instance of a world object
	2. Set the width and height of the world
	3. Set each tile to a random number between 0 and 100
	4. Numbers are averaged n times. Each average is the average of the tile and its 8 neighbours
	5. Tiles with a number greater than a threashold are made land tiles. Anything less is made water
	6. Checks are used to make sure that land tiles are all reachable
	7. Dungeons are added at random land tiles in the world. The final dungeon is always at the center
	8. Player spawn point is added at the center of the land

"""

########################################

# Imports

import random
import time
from Vector import Vector2
from Log import log as Log
from Log import save2DArray as Save2D

class World(object):
	"""
	World object
	"""
	## The 3D array of dungeon data. Each cell contains [character, color pair name]
	worldData = []

	## The maximum height of the dungeon
	sizeY = 0
	## The maximum width of the dungeon
	sizeX = 0
	## Name of the world
	name = ""
	## How many iterations
	iterations = 3

	## Class constructor
	def __init__(self, sizeX, sizeY, name = "Tenebrae Overworld", threashold = 40, seed = 0):
		"""
		Class constructor
		"""

		self.sizeX = sizeX
		self.sizeY = sizeY
		self.name = name
		self.threashold = threashold
		self.playerSpawn = Vector2(self.sizeX /2, self.sizeY /2)
		self.dungeonLocations = []
		self.seed = seed

		if(self.seed == 0):
			self.seed = int(random.random() * 27111995)
						
		Log("SEED: " + str(self.seed))

		self.worldData = self.CreateNew()

	## Returns a ground tile to use
	def getGroundTile(self):
		"""
		Returns a floor tile to use

		Return:
			A floor tile
		"""

		tiles = [" "] * 1 + ["^"] * 1 + ["."] * 1
		random.seed()
		return random.choice(tiles)

	## Returns a Vector2 of a valid dungeon location
	def GetDungeonLocation(self):
		"""
		Returns a Vector2 of a valid dungeon location

		Returns:
			A Vector2 of a valid dungeon location
		"""

		random.seed()

		x = random.randint(0, self.sizeX -1)
		y = random.randint(0, self.sizeY -1)

		# Check the tile data for this tile
		if(self.worldData[y][x][1] != "Water"):
			return Vector2(x, y)

		return self.GetDungeonLocation()

	## Adds the dungeon locations to the world
	def AddDungeons(self):
		"""
		Adds the dungeon locations to the world
		"""

		for loc in self.dungeonLocations:
			x = int(loc.x)
			y = int(loc.y)

			self.worldData[y][x] = [u"\u2206", "Dungeon", self.worldData[y][x][2]]


	## Create a new blank world
	def CreateNew(self):
		"""
		Creates a new blank world
		"""

		Log("Creating blank world")

		start = time.time()

		random.seed(self.seed)

		tempWorld = []

		for y in xrange(self.sizeY):
			tempWorld.append([])
			for x in xrange(self.sizeX):

				if(x == 0 or x == 1 or x == self.sizeX or x == self.sizeX -1):
					tempWorld[y].append(-100)

				elif(y == 0 or y == 1 or y == self.sizeY or y == self.sizeY -1):
					tempWorld[y].append(-100)

				else:
					tempWorld[y].append(random.randint(0, 100))

		end = time.time() - start
		msg = "Finished creating blank world. Took %fs" %(end)
		Log(msg)

		return tempWorld

	## Main world generation function
	def Generate(self):
		"""
		Generates a new world
		"""

		####################
		# Helper functions #
		####################

		def average(x, y):
			"""
			Averages a tile by its neighbours
			"""

			sums = []

			sums.append(self.worldData[y][x])


			# Add the lower tiles to the sum list
			if(y > 1):

				sums.append(self.worldData[y-1][x])

				if(x > 1):
					sums.append(self.worldData[y-1][x-1])
				if(x < self.sizeX -1):
					sums.append(self.worldData[y-1][x+1])

			# Add the higher tiles
			if(y < self.sizeY -1):
				sums.append(self.worldData[y+1][x])

				if(x > 1):
					sums.append(self.worldData[y+1][x-1])
				if(x < self.sizeX -1):
					sums.append(self.worldData[y+1][x+1])

			# Add the left and right tiles
			if(x > 1):
				sums.append(self.worldData[y][x-1])

			if(x < self.sizeX -1):
				sums.append(self.worldData[y][x+1])

			# Return the average
			return sum(sums) / len(sums)


		def iterate():
			"""
			One iteration of the algorithm
			"""

			tempWorld = []

			for y in xrange(self.sizeY):
				tempWorld.append([])
				for x in xrange(self.sizeX):
					tempWorld[y].append(average(x, y))

			for y in xrange(self.sizeY):
				for x in xrange(self.sizeX):
					self.worldData[y][x] = tempWorld[y][x]

		def convertToTiles():
			"""
			Converts a number to a tile, based on a threashold
			"""

			tempWorld = []

			for y in xrange(self.sizeY):
				tempWorld.append([])

				for x in xrange(self.sizeX):
					tileNo = self.worldData[y][x]

					if(tileNo > self.threashold):
						tempWorld[y].append([self.getGroundTile(), random.choice(["Dirt", "Grass"]), tileNo])

					else:
						tempWorld[y].append(["~", "Water", tileNo])

			
			self.worldData = tempWorld
			# for y in xrange(self.sizeY):
			# 	for x in xrange(self.sizeX):

			# 		msg = "Tile %i, %i: " + str(tempWorld[y][x])
			# 		Log(msg)

			# 		self.worldData[y][x] = tempWorld[y][x]



		####################

		start = time.time()

		for i in xrange(self.iterations):
			iterate()

		convertToTiles()

		end = time.time() - start
		msg = "Finished generating world. Took %fs" %(end)
		Log(msg)








