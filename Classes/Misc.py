##############################################
#											 #
# Miscellaneous functions, written by X.Hunt #
#									  		 #
##############################################

"""
Miscellaneous functions

NOTE: NEVER USED AT ALL

"""

############################################################################################

## Removes any None elements in an array
def removeNone(arr):
	"""
	Removes any None elements in an array.

	Parameters:
		- arr:			The array to use

	Returns:
		A copy of the array, but without any None elements
	"""

	newArr = []
	for i in xrange(len(arr) -1):
		if(arr[i] != None):
			newArr.append(arr[i])

	return newArr

############################################################################################

## Removes any None elements in an array
def removeNone2D(arr):
	"""
	Removes any None elements in an array.

	Parameters:
		- arr:			The 2D array to use

	Returns:
		A copy of the array, but without any None elements
	"""

	noneY = 0
	noneX = 0

	newArr = []

	for y in xrange(len(arr) -1):
		if(arr[y] != None):
			newArr.append(arr[y])
			for x in xrange(arr[y]):
				newArr[y].append(removeNone(arr[y]))


