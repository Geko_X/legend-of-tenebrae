######################################
#									 #
# GetItem  script, written by X.Hunt #
#									 #
######################################

"""
Gets and returns item from file
"""

# Imports
import sys, os
import random

from Log import log as Log

from Items.Weapons import *
from Items.Armors import *
from Items.Consumables import *

# Get the data and create list of items
filePath = os.path.dirname(sys.argv[0]) + "/Data/items.txt"
itemFile = open(filePath, "r")
items = itemFile.read().split("\n")

for i in xrange(len(items)):
	data = items[i]
	items[i] = data.split(",")


####################################################

## Generates a consumbale
def genConsumable(data):
	"""
	Generates a consumbale
	"""

	evalString = "%s('%s', %s, %i, %i, %i, %i, %i)" %(data[1], data[2], data[1], int(data[3]), int(data[4]), int(data[5]), int(data[6]), int(data[7]))
	newItem = eval(evalString)

	Log("New item created: " + str(newItem))

	return newItem

## Generates a gear
def genGear(data):
	"""
	Generates a gear
	"""
	evalString = "%s('%s', '%s', %i, %i, %i, %i)" %(data[1], data[2], data[3], int(data[4]), int(data[5]), int(data[6]), int(data[7]))
	newItem = eval(evalString)

	Log("New item created: " + str(newItem))

	return newItem

## Gets an item
def getItem(itemName):
	"""
	Gets an item from file
	"""

	for item in items:

		if(item[0] == itemName):

			# Consumable, itemType is one of the following:
			if(item[1] in ["Food", "HealthPotion", "RestorationScroll", "StatIncrease", "IncreasingScroll", "IncreasingPotion"]):
				return genConsumable(item)

			# Gear
			if(item[1] in ["Sword", "Mace", "Bow", "Shield", "Helm", "Chestplate", "Leggings", "Boots", "Gloves"]):
				return genGear(item)

			msg = "Could not create %s" %(itemName)
			Log(msg)

			return None

	Log("WARNING: Did not find item - " + str(itemName))
	return None

## Returns a random drop of any item
def getAnyItem():
	"""
	Returns a random drop of any item
	"""

	itemID = random.randint(0, len(items) -1)
	itemName = items[itemID][0]

	return getItem(itemName)


## Returns a lookupName from an item name
def getLookUpName(itemName):
	"""
	Returns a lookupName from an item name
	"""

	for item in items:
		if(item[2] == itemName):
			return item[0]

	Log("WARNING: Did not find item - " + str(itemName))
	return None



