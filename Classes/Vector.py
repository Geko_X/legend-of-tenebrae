# Vector class, written by X.Hunt
#############

# Handles classes for 2D vectors and 3D vectors
#############

###################################################################################################
#############################################         #############################################
############################################# Vector2 #############################################
#############################################         #############################################
###################################################################################################

import math

class Vector2(object):

	def __init__(self, x = 0, y = 0):

		self.x = float(x)
		self.y = float(y)

	def __str__(self):
		return "(%i, %i)" %(int(self.x), int(self.y))

	def __repr__(self):
		return "(%i, %i)" %(int(self.x), int(self.y))

	###############
	# COMPARISONS #
	###############

	# Equality
	def __eq__(self, other):
		if((self.x == other.x) & (self.y == other.y)): return True
		else: return False

	# Non-equality
	def __ne__(self, other):
		if((self.x == other.x) & (self.y == other.y)): return False
		else: return True

	# Less than
	def __lt__(self, other):
		if((self.x < other.x) & (self.y < other.y)): return True
		else: return False

	# Greater than
	def __gt__(self, other):
		if((self.x > other.x) & (self.y > other.y)): return True
		else: return False

	# Less than or equals to
	def __le__(self, other):
		if((self.x <= other.x) & (self.y <= other.y)): return True
		else: return False

	# Greater than or equals to
	def __gt__(self, other):
		if((self.x >= other.x) & (self.y >= other.y)): return True
		else: return False

	########################
	# BUILT IN MATHS STUFF #
	########################

	def __pos__(self):
		return self

	def __neg__(self):
		return Vector2(-self.x, -self.y)

	def __abs__(self):
		return Vector2(abs(self.x), abs(self.y))

	def __add__(self, other):

		if(type(other) == Vector2):
			return Vector2(self.x + other.x, self.y + other.y)
		else:
			return Vector2(self.x + other, self.y + other)

	def __sub__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x - other.x, self.y - other.y)
		else:
			return Vector2(self.x - other, self.y - other)

	def __mul__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x * other.x, self.y * other.y)
		else:
			return Vector2(self.x * other, self.y * other)

	def __floordiv__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x // other.x, self.y // other.y)
		else:
			return Vector2(self.x // other, self.y // other)

	def __div__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x / other.x, self.y / other.y)
		else:
			return Vector2(self.x / other, self.y / other)

	def __mod__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x % other.x, self.y % other.y)
		else:
			return Vector2(self.x % other, self.y % other)

	def __pow__(self, other):
		if(type(other) == Vector2):
			return Vector2(self.x ** other.x, self.y ** other.y)
		else:
			return Vector2(self.x ** other, self.y ** other)


	########################
	# MATHS WITH 2 VECTORS #
	########################

	def Add(v1, v2):
		return Vector2(v1.x + v2.x, v1.y + v2.y)

	def Subtract(v1, v2):
		return Vector2(v1.x - v2.x, v1.y - v2.y)

	def Multiply(v1, v2):
		return Vector2(v1.x * v2.x, v1.y * v2.y)

	def Divide(v1, v2):
		return Vector2(float(v1.x) / float(v2.x), float(v1.y) / float(v2.y))

	def Distance(v1, v2):
		return math.sqrt((v1.x * v2.x) + (v1.y * v2.y))

	def Min(v1, v2):
		x = v2.x if (v1.x > v2.x) else v1.x
		y = v2.y if (v1.y > v2.y) else v1.y
		return Vector2(x, y)

	def Max(v1, v2):
		x = v2.x if (v1.x < v2.x) else v1.x
		y = v2.y if (v1.y > v2.y) else v1.y
		return Vector2(x, y)

	def Magnitude(self):
		return self * self

	def SqrtMagnitude(self):
		x = math.sqrt(self.x * self.x)
		y = math.sqrt(self.y * self.y)
		return Vector2(x, y)

	#######################
	# MATHS WITH 1 VECTOR #
	#######################

	def AddSelf(self, n):
		return Vector2(self.x + n, self.y + n)

	def SubtractSelf(self, n):
		return Vector2(self.x - n, self.y - n)

	def MultiplySelf(self, n):
		return Vector2(self.x * n, self.y * n)

	def DivideSelf(self, n):
		return Vector2(float(self.x) / n, float(self.y) / n)

###################################################################################################
#############################################         #############################################
############################################# Vector3 #############################################
#############################################         #############################################
###################################################################################################

###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################

if __name__ == '__main__':

	v1 = Vector2(5, 3)
	v2 = Vector2(2, 4)

	print "=" * 10
	print "Vectors: %s, %s" %(v1, v2)
	print "=" * 10
	print "SIMPLE MATHS"
	print "=" * 10
	print "Adding two vectors: " + str((v1 + v2))
	print "Adding a number to a vector: " + str(v1) + " + 3 = " + str(v1 + 3)
	print "Subtracting two vectors: " + str((v1 - v2))
	print "Subtracting a number from a vector: " + str(v1) + " - 3 = " + str(v1 - 3)
	print "=" * 10
	print "MIN/MAX"
	print "=" * 10
	print "Min: " + str(Vector2.Min(v1, v2))
	print "Max: " + str(Vector2.Max(v1, v2))
	print "=" * 10
	print "DISTANCE and MAGNITUDE"
	print "=" * 10
	print "Distance: " + str(Vector2.Distance(v1, v2))
	print "Magnitude: " + str(Vector2.Magnitude(v1))
	print "SqrtMagnitude: " + str(Vector2.SqrtMagnitude(v1))








