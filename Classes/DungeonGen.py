########################################
#									   #
# Dungeon Generator, written by X.Hunt #
#									   #
########################################

"""
Behold, the all powerful dungeon generation algorithms of algorithmic generation of dungeons!!
Without this script, there will be no dungeons!

By the power of multiprocessing, algorithms are faster! (Or would be if it didnt break for no reason)

BASIC OUTLINE OF GENERATING DUNGEONS:

	1. Create a new instance of a dungeon object
	2. Set the type, width and height of the dungeon
	3. Generate a blank dungeon
	4. Using the algorithm of type dunType, create the dungeon
	5. Assign the generated dungeon to the instantiated dungeon object

DUNGEON TYPES:
	0 - Blank
	1 - Drunkard walk
	2 - Recursive division
	3 - Cellular automation

NOTE: All algorithms are explained in the docstrings of the functions used to generate them

Each type gives a unique and distinctive feel.

	0 - Set every cell to a floor cell.
		Every grid cell is walkable.

	1 - Uses a "drunkard's walk".
		Creates natural and "staggering" caves.

	2 - Divides rooms into smaller rooms, smaller rooms into even smaller rooms.
		Gives a man made, square-like feel.

	3 - Literally grow a maze from a starting point.
		Gives rounded dungeons with numerous paths, many loops, and few dead ends

NOTE:
As of submission time, only algorithms 0 and 1 have been implemented

"""

########################################

# Imports

import random
import time
from multiprocessing import Process, Queue, Manager
from Vector import Vector2
from Log import log as Log
from Log import save2DArray as Save2D
from SaveLoad import SaveDungeon
import Name


##########

## Dungeons are objects with a type, width, height and depth
class Dungeon(object):
	"""
	Dungeon object. This is used to generate the dungeon
	"""

	## The type of dungeon to generate
	dunType = 0

	## The 3D array of dungeon data. Each cell contains [character, color pair name]
	dunData = []

	## The maximum height of the dungeon
	sizeY = 0
	## The maximum width of the dungeon
	sizeX = 0
	## The depth the dungeon will be
	depth = 0
	## Current floor
	currentFloor = 0
	## Previous floor
	previousFloor = -1
	## Name of this dungeon. Created from a file
	name = ""
	## The location of this dungeon
	location = Vector2(0, 0)

	## Base spawn rate for monsters
	BASE_SPAWN_RATE = 0.05
	## Current spawn rate
	currentSpawnRate = BASE_SPAWN_RATE

	## Class constructor
	def __init__(self, dunType, sizeY, sizeX, depth, ID, name = "Dungeon", location = Vector2(0, 0), monsterList = []):
		"""
		Class constructor

		Parameters:
			- dunType:			The type of dungon to generate
			- sizeY:			The maximum height of the dungeon
			- sizeX:			The maximum width of the dungeon
			- depth:			The depth the dungeon will be
		"""

		self.dunData = []

		self.type = dunType
		self.sizeY = sizeY
		self.sizeX = sizeX
		self.depth = depth
		self.ID = ID
		self.name = Name.getName()
		self.location = location

		self.monsterList = ["Zombie", "Skeleton", "Rat", "Bat"]

		Log("Dungeon object instantiated")
		self.CreateNew()

	########################################

	## Returns a floor tile to use
	def getFloorTile(self):
		"""
		Returns a floor tile to use

		Return:
			A floor tile
		"""

		tiles = [" "] * 1 + ["^"] * 5 + ["."] * 5
		return random.choice(tiles)

	## Returns a floor tile to use
	def getWallTile(self):
		"""
		Returns a wall tile to use

		Return:
			A wall tile
		"""
		tiles = [" "] * 50 + ["#"] * 1 + ["/"] * 1 + ["\\"] * 1 + ["'"] * 1
		return random.choice(tiles)

	########################################

	## Sets the current floor of the dungeon
	def setCurrentFloor(self, f):
		"""
		Sets the current floor of the dungeon

		Parameters:
			- f:				The floor to set to

		Return:
			True:				If successful
			False:				If failed
		"""

		if(f <= self.depth and f > 0):
			self.previousFloor = self.currentFloor
			self.currentFloor = f
			return True

		else:
			Log("Could not set current floor: Floor not in dungeon")
			return False

	## Returns a 2D array for the current floor
	def getCurrentFloor(self):
		"""
		Returns a 2D array for the current floor

		Return:
			Current floor:		If successful
			False:				If failed
		"""

		if(self.currentFloor == 0):
			Log("Could not get floor: Dungeon not generated")
			return False

		return self.dunData[self.currentFloor]

	########################################

	## Gets the monster spawn rate for this dungeon floor
	def getSpawnRate(self):
		"""
		Returns a percentage that a battle will occur when the player moves
		"""

		self.currentSpawnRate = self.BASE_SPAWN_RATE * (self.currentFloor)
		return self.currentSpawnRate

	########################################

	## Saves the dungeon to a .dun file
	def Save(self):
		"""
		Saves a dungeon to a .dun file
		"""
		SaveDungeon(self)

	########################################

	## Create a new blank dungeon
	def CreateNew(self):
		"""
		Creates the intial blank maze

		Returns:
			A new blank maze
		"""

		# Set the "header" index to a blank array
		self.dunData.append([])

		for d in xrange(self.depth):

			# Create a new floor
			floor = []

			for y in xrange(self.sizeY):
				floor.append([])

				for x in xrange(self.sizeX):
					floor[y].append([self.getWallTile(), "Wall"])

			# Add the newly generated floor to the dungeon data
			self.dunData.append(floor)
			#Save2D(floor, "New" + str(d))

		Log("Created new dungeon")

	## Generate a new dungeon of dungeon type type with seed seed
	def Generate(self, seed = 0):
		"""
		Generate the dungeon with a given seed.
		If seed is 0, a random seed is used

		Paramters:
			- seed:			The seed to use. Random if 0

		Returns:
			Newly generated dungeon as a 3D array of [character, color]
		"""

		startTime = time.time()

		# If the seed is 0, randomise it
		if(seed == 0):
			seed = int(random.random() * 27111995)	# Magic number is my birthdate

		# Using the magic of eval(), we can change some of the code.
		# eval() evaluates a given string of literal python text
		algorithm = ""

		# Python doesnt have "proper" case selection, so this will have to do...

		# Blank type
		if(self.type == 0):
			algorithm = "self.genBlank"

		# Drunkard walk
		elif(self.type == 1):
			algorithm = "self.genDrunk"

		# Recursive division
		elif(self.type == 2):
			algorithm = "self.genDiv"

		# Cellular automation
		elif(self.type == 3):
			algorithm = "self.genCellAut"

		# Invalid type
		else:
			Log("Error generating dungeon: Invalid type")
			self.dunData = 0
			return

		# How many iterations
		iterations = int((self.sizeX + self.sizeY) * 250)

		# The queue to get all the created floors from threads
		q = Queue()

		# List of all the threads
		threadList = []

		## The new dungeon we are generating
		newDun = self.dunData

		## One quater of the height
		quaterY = int(self.sizeY / 4)
		## One quater of the width
		quaterX	= int(self.sizeX / 4)

		# Spawn threads, one for each floor
		for i in xrange(1, self.depth + 1):

			floor = newDun[i]

			# The start point. This will always be in the upper left quadrant
			startPoint = Vector2(1 + random.randint(1, quaterX), 1 + random.randint(1, quaterX))
			# The end point. This will always be in the lower right quadrant
			endPoint = Vector2(random.randint(quaterX, self.sizeX -1) -1, random.randint(quaterY * 3, self.sizeY -1) -1)

			#thread = Process(target = eval(algorithm), args = (seed, floor, iterations, startPoint, endPoint, q))
			#thread.start()

			eval(algorithm + "(seed, floor, iterations, startPoint, endPoint, q)")

		# Start the processes
		#for t in threadList:
		#	t.start()

		# Retreive the results from each process
		for i in xrange(1, self.depth + 1):

			data = q.get()

			newDun[i] = data[0]

			startX = int(data[1].x)
			startY = int(data[1].y)

			startPoint = Vector2(startX, startY)

			endX = int(data[2].x)
			endY = int(data[2].y)

			endPoint = Vector2(endX, endY)
			
			newDun[i][startY][startX] = [u"\u2206", "Start"]
			newDun[i][endY][endX] = [u"\u2207", "End"]
			newDun[0].append([startPoint, endPoint])

		# Stop the processes
		for t in threadList:
			t.terminate()

		endTime = time.time() - startTime
		Log("Finished creating dungeon. Took " + str(endTime) + "s")

		self.dunData = newDun
		self.setCurrentFloor(1)

		

	################################
	# Dungen generation algorithms #
	################################

	## Generates a blank dungeon. Every cell is walkable except for the outer walls
	def genBlank(self, seed, floor, iterations, startPoint, endPoint, q):
		"""
		Sets every cell to a walkable floor cell. Outer cells remain as walls

		Returns:
			Blank dungeon as a 3D array of [character, color]
		"""


		## The current floor
		floor = []

		for y in xrange(self.sizeY):
			floor.append([])

			for x in xrange(self.sizeX):
				floor[y].append([self.getFloorTile(), "Floor"])

				if((y == 0) or (y == self.sizeY -1) or (x == 0) or (x == self.sizeX -1)):
					floor[y][x] = [self.getWallTile(), "Wall"]


		q.put([floor, startPoint, endPoint])

		Log("Created blank dungeon floor")

	## Generates a new dungeon using the Drunkard's Walk algorithm
	def genDrunk(self, seed, floor, iterations, startPoint, endPoint, q):
		"""
		Uses a "dunkard's walk" to generate natural and "staggering" caves.

		EXPLINATION:

			1. Pick a start and an end point

			2. Set the current cell to be the start point

			3. Set the current cell as a floor cell

			4. From the current point, choose a direction to move
				- This direction is biased towards the end point for first pass
				- The direction will not be into a boundary

			5. Set the current cell to be the current cell + the direction to move

		Parameters:
			- seed:			The seed to use
			- floor:		The array to save to
			- iterations:	How many iterations
			- startPoint:	The starting point
			- endPoint:		The ending point
			- q:			The queue to push the result to, which is gotten when all processes are done

		Returns:
			New floor as a 2D array of [character, color]
		"""

		random.seed(seed)

		## The current cell
		currentCell = Vector2(0, 0)
		currentCell.y = startPoint.y
		currentCell.x = startPoint.x

		for i in xrange(0, iterations):

			# Set the current cell to a wall cell if it is on the edge, otherwise a floor cell
			if(int(currentCell.y) == 0) or (int(currentCell.y) == self.sizeY -1) or (int(currentCell.x) == 0) or (int(currentCell.x) == self.sizeX -1):
				floor[int(currentCell.y)][int(currentCell.x)] = [" ", "Wall"]

			else:
				floor[int(currentCell.y)][int(currentCell.x)] = [self.getFloorTile(), "Floor"]

			# Get the weights for each direction, checking that things are within the bounds of the dungeon
			if(currentCell.x <= 0):
				plusX = 5

			elif(currentCell.x >= self.sizeX -1):
				plusX = 0

			else:
				if(currentCell.x - endPoint.x < 0):
					plusX = 3
				else:
					plusX = 2

			
			if(currentCell.y <= 0):
				plusY = 5

			elif(currentCell.y >= self.sizeY -1):
				plusY = 0

			else:
				if(currentCell.y - endPoint.y < 0):
					plusY = 3
				else:
					plusY = 2

			minusX = 5 - plusX
			minusY = 5 - plusY

			# List of probabilities
			weightedDirection = [Vector2(1, 0)] * plusX + [Vector2(-1, 0)] * minusX + [Vector2(0, 1)] * plusY + [Vector2(0, -1)] * minusY

			# The direction to move in
			direction = random.choice(weightedDirection)

			currentCell += direction

		# This is now done, put the results in the queue
		q.put([floor, startPoint, endPoint])

		Log("Created drunk dungeon floor")













