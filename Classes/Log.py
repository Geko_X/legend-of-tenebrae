#############################
#							#
# Log.py, written by X.Hunt #
#							#
#############################

"""
Handles all log functions, used for debugging
"""

import sys, os
from datetime import datetime as dt

def log(message):
	"""
	Logs a message with a timestamp to the Logs folder
	"""
	message = str(message)

	## Path of the Logs folder
	logPath = os.path.dirname(sys.argv[0]) + "/Logs/Log.log"
	## The current time
	now = dt.now()
	## The data to write. It is the time + the message
	data = str(now) + " - " + message + "\n"

	# Open, append to, and close the file
	with open(logPath, "a") as logFile:
		logFile.write(data)

def save2DArray(arr, fileName = ""):
	"""
	Saves a 2D array to a given file
	"""

	fileName = str(fileName)

	filePath = os.path.dirname(sys.argv[0]) + "/Logs/%s.txt" %(fileName + " - "  + str(dt.now()))

	with open(filePath, "w") as arrFile:
		for y in xrange(len(arr) -1):

			line = ""

			for x in xrange(len(arr[y]) -1):
				s = arr[y][x][0]
				line += (s + " ")

			line += "\n"
			arrFile.write(line.encode("utf-8"))


###############################

# Clears the logfile
logPath = os.path.dirname(sys.argv[0]) + "/Logs/Log.log"
logFile = open(logPath, "w")
logFile.close()

log("=" * 50)
log("Starting log")