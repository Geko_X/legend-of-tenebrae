#####################################
#									#
# Name Generator, written by X.Hunt #
#									#
#####################################

"""
Name generator script to generate names.

Reads an adjective from file and then a noun from file. Simple.
"""

import os, sys
import random


dataPath = os.path.dirname(sys.argv[0]) + "/Data/"
adjPath = dataPath + "dungeonAdjectives.txt"
nounPath = dataPath + "dungeonNouns.txt"

with open(adjPath, "r") as f:
	adj = f.read().split("\n")

with open(nounPath, "r") as f:
	noun = f.read().split("\n")

## Returns a new random name
def getName():
	"""
	Returns a new random name
	"""

	name = random.choice(adj) + " " + random.choice(noun)
	return name